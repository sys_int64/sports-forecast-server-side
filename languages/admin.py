from django.contrib import admin
from singlemodeladmin import SingleModelAdmin
from languages.models import Languages, Language
from django.conf import settings


class StackedLanguageInline(admin.StackedInline):
    template = "admin/widgets/language_tabular_inline.html"


class TabularLanguageInline(admin.TabularInline):
    template = "admin/widgets/language_tabular_inline.html"


class SingleLanguageInline(admin.TabularInline):
    template = "admin/widgets/language_single_inline.html"


class LanguageInline(admin.TabularInline):
    model = Language
    fields = ('country_code', 'country_name', 'will_win_text', 'will_draw_text')
    extra = 1


class SettingsAdmin(SingleModelAdmin):
    inlines = [LanguageInline]


admin.site.register(Languages, SettingsAdmin)
