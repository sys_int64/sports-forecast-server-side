from django.db import models


class Languages(models.Model):
    class Meta:
        verbose_name = 'Настройки'
        verbose_name_plural = 'Настройки'

    football_enabled = models.BooleanField("Включен футбол", default=True)
    basketball_enabled = models.BooleanField("Включен баскетбол", default=True)
    tennis_enabled = models.BooleanField("Включен теннис", default=True)
    hockey_enabled = models.BooleanField("Включен хоккей", default=True)


class Language(models.Model):
    languages = models.ForeignKey(Languages)
    country_code = models.CharField("Код страны", max_length=4, primary_key=True)
    country_name = models.CharField("Название", max_length=255, default="")
    will_win_text = models.CharField("Выйграют", max_length=255, default="", blank=True)
    will_draw_text = models.CharField("Ничья", max_length=255, default="", blank=True)

    def __str__(self):
        return self.country_name
