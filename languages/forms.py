from django import forms
from languages.models import Language
from django.template import loader


class LanguagesSelectInput(forms.Widget):

    class Media:
        css = {
            'all': (
                    'admin/css/flags.css',
                    'admin/css/widgets.css',
            )
        }

    def render(self, name, value, attrs=None):

        context = {
            "languages": Language.objects.all(),
            "active_code": "us",
            "blanks": [],
        }

        template = loader.get_template('admin/widgets/language_select_input.html')
        return template.render(context)


class LanguageInput(forms.Widget):

    class Media:
        css = {
            'all': (
                    'admin/css/flags.css',
                    'admin/css/widgets.css',
            )
        }

        js = ('admin/js/languages.js',)

    def render(self, name, value, attrs=None):
        if (value == None):
            language = ""
        else:
            language = value

        context = {
            "name": name,
            "value": value,
            "language": language,
        }

        template = loader.get_template('admin/widgets/language_input.html')
        return template.render(context)
