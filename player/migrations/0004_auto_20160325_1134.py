# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-03-25 11:34
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('languages', '0003_auto_20160324_1234'),
        ('player', '0003_auto_20160321_1245'),
    ]

    operations = [
        migrations.CreateModel(
            name='PlayerTranslation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, verbose_name='Название')),
                ('text', models.TextField(blank=True, verbose_name='Описание')),
                ('language', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='player_lang', to='languages.Language', verbose_name='Язык')),
            ],
        ),
        migrations.RemoveField(
            model_name='player',
            name='text',
        ),
        migrations.RemoveField(
            model_name='player',
            name='title',
        ),
        migrations.AddField(
            model_name='playertranslation',
            name='player',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='player.Player'),
        ),
    ]
