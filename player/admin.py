from django.contrib import admin
from player.models import Player, PlayerTranslation
from languages.admin import SingleLanguageInline
from player.forms import PlayerForm, PlayerTranslationForm
from django.db.models.query_utils import Q
from userprofile.models import UserProfile


class TranslationInline(SingleLanguageInline):
    model = PlayerTranslation
    extra = 0
    form = PlayerTranslationForm
    max_num = 999


class PlayerAdmin(admin.ModelAdmin):
    def title(self, obj):
        return obj.__str__()

    title.allow_tags = True
    title.short_description = "Название"

    list_display = ('title', 'sport',)
    list_filter = ['sport',]
    #search_fields = ['title', 'text']
    inlines = [TranslationInline]
    form = PlayerForm
    search_fields = [
        'playertranslation__title',
        'playertranslation__text',
    ]

    def get_queryset(self, request):
        qs = super(PlayerAdmin, self).get_queryset(request)
        perm_query = UserProfile.get_query_sportperm(request)
        return qs.filter(perm_query)


admin.site.register(Player, PlayerAdmin)
