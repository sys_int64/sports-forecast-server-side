from django import forms
from string import Template
from django.utils.safestring import mark_safe
from player.models import Player, PlayerTranslation
from django.template import loader
from languages.forms import LanguagesSelectInput, LanguageInput
from languages.models import Language


class PlayerForm(forms.ModelForm):

    class Meta:
        model = Player
        fields = '__all__'

    lang = forms.ModelChoiceField(Language.objects.all(), label="Язык",
                                  required=False, widget=LanguagesSelectInput)


class PlayerTranslationForm(forms.ModelForm):

    class Meta:
        model = PlayerTranslation
        #fields = '__all__'
        fields = ('language', 'title',)

    language = forms.ModelChoiceField(Language.objects.all(), label="Язык",
                                      required=True, widget=LanguageInput)
