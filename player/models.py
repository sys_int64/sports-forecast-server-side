from django.db import models
from sorl.thumbnail import ImageField, get_thumbnail
from django.conf import settings
from languages.models import Language


class Player(models.Model):

    SPORT_CHOICES = (
        ('0', 'Футбол'),
        ('1', 'Баскетбол'),
        ('2', 'Теннис'),
        ('3', 'Хоккей'),
    )

    class Meta:
        verbose_name = "Команда"
        verbose_name_plural = "Комнады"

    sport = models.CharField("Спорт", max_length=1, choices=SPORT_CHOICES,
                             default='0')
    image = ImageField("Картинка", null=True)

    def get_translation(self, lang):
        translations = self.playertranslation_set.all()
        en_translation = None

        for translation in translations:
            if translation.language.pk == lang:
                return translation
            elif translation.language.pk == "us":
                en_translation = translation

        return en_translation

    def __str__(self):
        translations = self.playertranslation_set.all()
        ru_title = ""
        en_title = ""

        for translation in translations:
            if translation.language.pk == "ru":
                ru_title = translation.title
            elif translation.language.pk == "us":
                en_title = translation.title

        if en_title != "":
            return en_title

        if ru_title != "":
            return ru_title

        return str(self.pk)

    def serialize_translations(self):

        translations = []

        for translation in self.playertranslation_set.all():
            translations.append({
                "lang": translation.language.pk,
                "title": translation.title
            })

        return {
            "translations": translations
        }

    def serialize(self, lang):
        # image = "{0}{1}".format (settings.HOST_URL, self.image.url)
        translation = self.get_translation(lang)
        # im = get_thumbnail()
        im = get_thumbnail(self.image, '512x512')
        image = "{0}{1}".format(settings.HOST_URL, im.url)

        im = get_thumbnail(self.image, '128x128')
        thumb = "{0}{1}".format(settings.HOST_URL, im.url)

        if (translation is None):
            title = ""
        else:
            title = translation.title

        return {
            "id": self.pk,
            # "title": self.title,
            "title": title,
            "image": image,
            "thumb": thumb,
            "text": "",
            # "text" : self.text,
        }


class PlayerTranslation(models.Model):
    player = models.ForeignKey(Player)
    language = models.ForeignKey(Language, related_name="player_lang",
                                 verbose_name="Язык", null=True, blank=True)
    title = models.CharField("Название", max_length=200)
    text = models.TextField("Описание", blank=True)
