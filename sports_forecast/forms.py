from django import forms

from express.models import ExpressTranslation, Express
from languages.forms import LanguageInput, LanguagesSelectInput
from languages.models import Language


class ExpressForm(forms.ModelForm):

    class Meta:
        model = Express
        fields = '__all__'

    lang = forms.ModelChoiceField(Language.objects.all(), label="Язык",
                                  required=False, widget=LanguagesSelectInput)


class ExpressTranslationForm(forms.ModelForm):

    class Meta:
        model = ExpressTranslation
        fields = ('language', 'title',)

    language = forms.ModelChoiceField(Language.objects.all(), label="Язык",
                                      required=True, widget=LanguageInput)
