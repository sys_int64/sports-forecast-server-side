from PIL import Image, ImageColor
from sorl.thumbnail.engines.pil_engine import Engine


class Engine(Engine):
    def create(self, image, geometry, options):
        thumb = super(Engine, self).create(image, geometry, options)

        try:
            background = Image.new('RGB', thumb.size, (255, 255, 255))
            background.paste(thumb, mask=thumb.split()[3])  # 3 is the alpha of an RGBA image.
            return background
        except:
            return thumb

        # if options.get('background'):
        #     try:
        #         background = Image.new('RGB', thumb.size, ImageColor.getcolor(options.get('background'), 'RGB'))
        #         background.paste(thumb, mask=thumb.split()[3]) # 3 is the alpha of an RGBA image.
        #         return background
        #     except Exception, e:
        #         return thumb
        # return thumb
