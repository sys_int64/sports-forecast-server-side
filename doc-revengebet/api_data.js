define({ "api": [
  {
    "type": "post",
    "url": "/device.auth/",
    "title": "Авторизация устройства",
    "name": "Auth",
    "group": "Device",
    "description": "<p>Авторизация устройства, либо регистрация, если <code>device_id</code> не найден.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client_id",
            "description": "<p>Уникальный номер клиента</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client_secret",
            "description": "<p>Секретный ключ клиента</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_id",
            "description": "<p>Идентификатор устройства</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apns_token",
            "description": "<p>APNS токен</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>True, если ошибок нет</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/auth/login.py",
    "groupTitle": "Device"
  },
  {
    "type": "post",
    "url": "/forecast.buy/",
    "title": "Купить пакет подсказок",
    "name": "ForecastBuy",
    "group": "Forecast",
    "description": "<p>Осуществляется покупка пакета подсказок по <code>packet_id</code> для пользователя с устройством <code>device_id</code>.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "device_id",
            "description": "<p>Уникальный идентификатор прогноза.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "packet_id",
            "description": "<p>Уникальный идентификатор пакета.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ok",
            "description": "<p>true, если покупка соверешена успешно.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/forecast/buy.py",
    "groupTitle": "Forecast"
  },
  {
    "type": "get",
    "url": "/forecast.get/",
    "title": "Список прогнозов",
    "name": "ForecastGet",
    "group": "Forecast",
    "description": "<p>Возвращает список прогнозов либо один прогноз. Для получения списка записей, небходимо указать начало выборки определенного подмножества записей - <code>offset</code> и количество записей, которое необходимо получить - <code>count</code>. Чтобы получить одну конкретную запись, необходимо указать уникальный идентификатор записи - <code>id</code>.<br> Если заполнено поле <code>sport</code>, метод вернет только записи, принадлежащие определенному виду спорта, которое записано в <code>sport</code>.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Уникальный идентификатор прогноза.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "description": "<p>Смещение, необходимое для выборки определенного подмножества записей.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "count",
            "description": "<p>Количество записей, которое необходимо получить (но не более 100).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "sport",
            "description": "<p>Вывести записи только одного вида спорта<br>    <b>0</b> - Футбол<br>    <b>1</b> - Баскетбол<br>    <b>2</b> - Тенис<br>    <b>3</b> - Волейбол</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Уникальный идентификатор прогноза.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "text",
            "description": "<p>Прогноз</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "odd",
            "description": "<p>Коэфициент</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sport",
            "description": "<p>Вид спорта<br>    <b>0</b> - Футбол<br>    <b>1</b> - Баскетбол<br>    <b>2</b> - Тенис<br>    <b>3</b> - Волейбол</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "date",
            "description": "<p>Дата</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "time",
            "description": "<p>Время</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "home",
            "description": "<p>Первая команда, запись <a href=\"#api-Player-PlayerGet\">Player</a>.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "away",
            "description": "<p>Вторая команда.</p>"
          },
          {
            "group": "Success 200",
            "type": "[Tip]",
            "optional": false,
            "field": "tips",
            "description": "<p>Массив с <a href=\"#api-Tip-TipGet\">подсказками</a></p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "next",
            "description": "<p>Следующий платный прогноз</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример",
          "content": "{\n    \"next\": {\n        \"odd\": 4.0,\n        \"tips\": 2,\n        \"id\": 19\n    },\n    \"forecast\": {\n        \"sport\": \"0\",\n        \"time\": \"10:47\",\n        \"date\": \"Февраль 10:47\",\n        \"tips\": [\n            {\n                \"text\": \"ASDASDasd\\r\\nasdasd\",\n                \"id\": 1,\n                \"forecast_id\": 1\n            },\n            {\n                \"text\": \"asdasdaf\\r\\nsdfsdf\",\n                \"id\": 2,\n                \"forecast_id\": 1\n            }\n        ],\n        \"id\": 1,\n        \"home\": {\n            \"title\": \"Test\",\n            \"id\": 1,\n            \"text\": \"Test\",\n            \"image\": \"https://pure-gorge-94712.herokuapp.com/media/icon1-01.png\"\n        },\n        \"away\": {\n            \"title\": \"Test 2\",\n            \"id\": 2,\n            \"text\": \"Test 2\\r\\nTest 2\",\n            \"image\": \"https://pure-gorge-94712.herokuapp.com/media/rouble.png\"\n        },\n        \"odd\": 1.0\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/forecast/get.py",
    "groupTitle": "Forecast"
  },
  {
    "type": "post",
    "url": "/forecast.open/",
    "title": "Разблокировать подсказку",
    "name": "ForecastOpen",
    "group": "Forecast",
    "description": "<p>Открывает заблокированную подсказку.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "device_id",
            "description": "<p>Уникальный идентификатор прогноза.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Уникальный идентификатор прогноза.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>Язык</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ok",
            "description": "<p>true, если покупка соверешена успешно.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/forecast/open.py",
    "groupTitle": "Forecast"
  },
  {
    "type": "post",
    "url": "/forecast.subscribe/",
    "title": "Оформить подписку",
    "name": "ForecastSubscribe",
    "group": "Forecast",
    "description": "<p>Оформить подписку на подсказки к спорту.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "device_id",
            "description": "<p>Уникальный идентификатор прогноза.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "sport",
            "description": "<p>Спорт, на который подписаться.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ok",
            "description": "<p>true, если покупка соверешена успешно.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/forecast/subscribe.py",
    "groupTitle": "Forecast"
  },
  {
    "type": "get",
    "url": "/history.get/",
    "title": "История игр",
    "name": "HistoryGet",
    "group": "History",
    "description": "<p>Возвращает список предыдущих игр. Для получения списка записей, небходимо указать начало выборки определенного подмножества записей - <code>offset</code> и количество записей, которое необходимо получить - <code>count</code>.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "description": "<p>смещение, необходимое для выборки определенного подмножества записей.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "count",
            "description": "<p>количество записей, которое необходимо получить (но не более 100).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "sport",
            "description": "<p>Вывести записи только одного вида спорта<br>    <b>0</b> - Футбол<br>    <b>1</b> - Баскетбол<br>    <b>2</b> - Тенис<br>    <b>3</b> - Волейбол</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Уникальный идентификатор.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "win",
            "description": "<p>true, если выйграл</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "forecast",
            "description": "<p>Модель <a href=\"#api-Forecast-ForecastGet\">Forecast</a>.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/history/get.py",
    "groupTitle": "History"
  },
  {
    "type": "get",
    "url": "/player.get/",
    "title": "Список команд",
    "name": "PlayerGet",
    "group": "Player",
    "description": "<p>Возвращает список команд либо одина команда. Для получения списка записей, небходимо указать начало выборки определенного подмножества записей - <code>offset</code> и количество записей, которое необходимо получить - <code>count</code>. Чтобы получить одну конкретную запись, необходимо указать уникальный идентификатор записи - <code>id</code></p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Уникальный идентификатор команды.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "description": "<p>смещение, необходимое для выборки определенного подмножества записей.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "count",
            "description": "<p>количество записей, которое необходимо получить (но не более 100).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Уникальный идентификатор команды.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Название команды</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "text",
            "description": "<p>Описание команды</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image",
            "description": "<p>Герб команды</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример",
          "content": "{\n    \"player\": {\n        \"id\": 2,\n        \"title\": \"Test 2\",\n        \"text\": \"Test 2\\r\\nTest 2\",\n        \"image\": \"https://pure-gorge-94712.herokuapp.com/media/rouble.png\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/player/get.py",
    "groupTitle": "Player"
  },
  {
    "type": "get",
    "url": "/tariffs.get/",
    "title": "Список тарифов",
    "name": "TariffsGet",
    "group": "Tariffs",
    "description": "<p>Возвращает список доступных тарифов. Также показывает количство оставшихся подсказок</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Уникальный идентификатор подсказки.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "left_tips",
            "description": "<p>Оставшиеся подсказки у пользователя</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Наименование пакета/подписки</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "tips",
            "description": "<p>Количество подсказок в пакете</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>Цена пакета/подписки</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример",
          "content": "\n{\n    \"left_tips\": 2,\n    \"tariffs\": {\n        \"packets\": [\n            {\n                \"id\": 1,\n                \"tips\": 10,\n                \"price\": 2.0,\n                \"title\": \"Novice\"\n            },\n            {\n                \"id\": 2,\n                \"tips\": 20,\n                \"price\": 4.0,\n                \"title\": \"Middle\"\n            },\n            {\n                \"id\": 3,\n                \"tips\": 40,\n                \"price\": 6.0,\n                \"title\": \"Pro\"\n            }\n        ],\n        \"subscribes\": [\n            {\n                \"id\": 1,\n                \"price\": 10.0,\n                \"title\": \"Football\"\n            },\n            {\n                \"id\": 2,\n                \"price\": 20.0,\n                \"title\": \"Basketball\"\n            },\n            {\n                \"id\": 3,\n                \"price\": 30.0,\n                \"title\": \"Tennis\"\n            },\n            {\n                \"id\": 4,\n                \"price\": 40.0,\n                \"title\": \"Volleyball\"\n            }\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/tariffs/get.py",
    "groupTitle": "Tariffs"
  },
  {
    "type": "get",
    "url": "/tip.get/",
    "title": "Список подсказок",
    "name": "TipGet",
    "group": "Tip",
    "description": "<p>Возвращает список подсказок либо одну подсказку. Для получения списка записей, небходимо указать начало выборки определенного подмножества записей - <code>offset</code> и количество записей, которое необходимо получить - <code>count</code>. Чтобы получить одну конкретную запись, необходимо указать уникальный идентификатор записи - <code>id</code></p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Уникальный идентификатор подсказки.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "description": "<p>смещение, необходимое для выборки определенного подмножества записей.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "count",
            "description": "<p>количество записей, которое необходимо получить (но не более 100).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Уникальный идентификатор подсказки.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "forecast_id",
            "description": "<p>Уникальный идентификатор прогноза, к которому привязана подсказка</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "text",
            "description": "<p>Текст подсказки</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример",
          "content": "{\n    \"tip\": {\n        \"forecast_id\": 1,\n        \"id\": 1,\n        \"text\": \"ASDASDasd\\r\\nasdasd\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>Код ошибки</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./api/tip/get.py",
    "groupTitle": "Tip"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./doc/main.js",
    "group": "_home_int64_dev_pure_gorge_94712_doc_main_js",
    "groupTitle": "_home_int64_dev_pure_gorge_94712_doc_main_js",
    "name": ""
  }
] });
