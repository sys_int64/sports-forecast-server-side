from django.contrib import admin
from singlemodeladmin import SingleModelAdmin
from django.conf import settings
from tariffs.models import *
from languages.admin import TabularLanguageInline


class TariffsTipsInline(TabularLanguageInline):
    model = TariffsTips
    extra = 3
    max_num = 3


class TariffsSubscribeInline(TabularLanguageInline):
    model = TariffsSubscribe
    extra = 4
    max_num = 4


class TariffsAdmin(SingleModelAdmin):
    inlines = [TariffsTipsInline, TariffsSubscribeInline]


admin.site.register(Tariffs, TariffsAdmin)
