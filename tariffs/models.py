from django.db import models


class Tariffs(models.Model):

    def serialize(self):
        tips = []
        subscribe = []

        for tariff in self.tariffstips_set.all():
            tips.append(tariff.serialize())

        for tariff in self.tariffssubscribe_set.all():
            subscribe.append(tariff.serialize())

        return {
            "packets": tips,
            "subscribes": subscribe,
        }


class TariffsTips(models.Model):
    tarrifs = models.ForeignKey(Tariffs)
    title = models.CharField("Название", max_length=200)
    tips = models.CharField("Tips", max_length=4)
    price = models.CharField("Цена", max_length=10)

    def serialize(self):
        return {
            "id": self.pk,
            "title": self.title,
            "tips": int(self.tips),
            "price": float(self.price),
        }


class TariffsSubscribe(models.Model):
    tarrifs = models.ForeignKey(Tariffs)
    title = models.CharField("Название", max_length=200)
    price = models.CharField("Цена $/мес.", max_length=10)

    def serialize(self):
        return {
            "id": self.pk,
            "title": self.title,
            "price": float(self.price),
        }
