from django import forms

from express.models import Express
from forecast.models import Forecast, Tip
from string import Template
from django.utils.safestring import mark_safe
from player.models import Player
from django.template import loader
from languages.forms import LanguagesSelectInput, LanguageInput
from languages.models import Language


class ForecastInput(forms.Widget):
    def render(self, name, value, attrs=None):
        if value is not None:
            forecast = Forecast.objects.get(pk=int(value))

            context = {
                "name": name,
                "value": value,
                "forecast": forecast,
            }
        else:
            context = {
                "name": name,
                "value": "",
            }

        template = loader.get_template('admin/widgets/forecast_info.html')
        return template.render(context)


class ExpressInput(forms.Widget):
    def render(self, name, value, attrs=None):
        if value is not None:
            express = Express.objects.get(pk=int(value))

            context = {
                "name": name,
                "value": value,
                "express": express,
            }
        else:
            context = {
                "name": name,
                "value": "",
            }

        template = loader.get_template('admin/widgets/express_info.html')
        return template.render(context)


class HistoryForm(forms.ModelForm):
    class Meta:
        model = Forecast
        fields = '__all__'
        exclude = ("datetime", )

    forecast = forms.ModelChoiceField(
        Forecast.objects.all(),
        label="Прогноз",
        required=False,
        widget=ForecastInput
    )

    express = forms.ModelChoiceField(
        Express.objects.all(),
        label="Экспресс",
        required=False,
        widget=ExpressInput
    )
