# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-03-24 05:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('history', '0003_auto_20160322_1025'),
    ]

    operations = [
        migrations.AlterField(
            model_name='history',
            name='price',
            field=models.FloatField(default='0', verbose_name='Ставка'),
        ),
        migrations.AlterField(
            model_name='history',
            name='win',
            field=models.BooleanField(default=True, verbose_name='Победил'),
        ),
    ]
