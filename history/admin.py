import urllib.parse

from django.conf.urls import url
from django.contrib import admin
from django.contrib import messages
from django.http import HttpResponseRedirect

from history.models import History
from django.db.models.query_utils import Q
from userprofile.models import UserProfile
from forecast.models import Forecast
from history.forms import HistoryForm


class ForecastInline(admin.StackedInline):
    model = Forecast
    can_delete = False
    verbose_name_plural = 'forecasts'


class HistoryAdmin(admin.ModelAdmin):
    class Media:
        css = {
            'all': ('admin/css/history_admin.css',)
        }

    def win(self, request, pk):
        try:
            history = History.objects.get(pk=pk)
            history.win = True
            history.approved = True
            history.save()

            messages.success(request, "Success")
        except History.DoesNotExist:
            messages.error(request, "History object does not found")

        return HttpResponseRedirect('../../?%s' % self.get_params(request))

    def lose(self, request, pk):
        try:
            history = History.objects.get(pk=pk)
            history.win = False
            history.approved = True
            history.save()

            messages.success(request, "Success")
        except History.DoesNotExist:
            messages.error(request, "History object does not found")

        return HttpResponseRedirect('../../?%s' % self.get_params(request))

    def return_bets(self, request, pk):
        try:
            history = History.objects.get(pk=pk)
            history.win = False
            history.revert = True
            history.approved = True
            history.save()

            messages.success(request, "Success")
        except History.DoesNotExist:
            messages.error(request, "History object does not found")

        return HttpResponseRedirect('../../?%s' % self.get_params(request))

    def get_urls(self):
        urls = super(HistoryAdmin, self).get_urls()
        urlpatterns = [
            url(r'^win/(?P<pk>[A-z0-9_]+)/$', self.win),
            url(r'^lose/(?P<pk>[A-z0-9_]+)/$', self.lose),
            url(r'^return_bets/(?P<pk>[A-z0-9_]+)/$', self.return_bets)
        ]

        return urlpatterns + urls

    def sport(self, obj):
        return obj.forecast.sport

    def save_model(self, request, obj, form, change):
        if obj.revert:
            obj.forecast.odd = 1.0
            obj.forecast.save()

        obj.save()

    def changelist_view(self, request, extra_context=None):
        self.request = request
        return super(HistoryAdmin, self) \
            .changelist_view(request, extra_context=extra_context)

    def get_params(self, request):
        params = []
        request.GET._mutable = True

        for k, vals in request.GET.lists():
            params.append((k, vals[0]))

        return urllib.parse.urlencode(params)

    def action(self, obj):
        return "<a href='win/{pk}/?params'>win</a><br>" \
               "<a href='lose/{pk}/?params'>lose</a><br>" \
               "<a href='return_bets/{pk}/?params'>return bets</a>"\
            .format(pk=obj.pk, params=self.get_params(self.request))

    def tips(self, obj):
        content = ""

        if obj.forecast is not None:
            for tip in obj.forecast.tip_set.filter(language="us"):
                content += tip.text+"<br>"

        if obj.express is not None:
            for forecast in obj.express.forecast_set.all():
                content += "<b>{}</b><br>".format(str(forecast))
                for tip in forecast.tip_set.filter(language="us"):
                    content += tip.text+"<br>"
                content += "<br>"

        return content

    def get_row_css(self, obj, index):
        if obj.approved:
            return "approved"

        return ""

    action.allow_tags = True
    action.short_description = "Action"

    tips.allow_tags = True
    tips.short_description = "Tips"

    sport.allow_tags = True
    sport.short_description = "Sport"
    #form = ForecastForm
    list_filter = ['forecast__sport', 'forecast__datetime', 'approved']
    list_display = ("__str__", "datetime", "tips", "action")
    #exclude = ("forecast", )
    form = HistoryForm
    #raw_id_fields = ("forecast", )
    #inlines = (ForecastInline, )

    def get_queryset(self, request):
        qs = super(HistoryAdmin, self).get_queryset(request)
        #perm_query = UserProfile.get_query_sportperm(request)
        perm_query = Q()
        user = request.user

        if user.has_perm("userprofile.can_football_manage"):
            perm_query |= Q(forecast__sport=Forecast.SPORT_FOOTBALL) | Q(forecast=None)

        if user.has_perm("userprofile.can_basketball_manage"):
            perm_query |= Q(forecast__sport=Forecast.SPORT_BASKETBALL) | Q(forecast=None)

        if user.has_perm("userprofile.can_volleyball_manage"):
            perm_query |= Q(forecast__sport=Forecast.SPORT_VOLLEYBALL) | Q(forecast=None)

        if user.has_perm("userprofile.can_volleyball_manage"):
            perm_query |= Q(forecast__sport=Forecast.SPORT_TENNIS) | Q(forecast=None)

        return qs.filter(perm_query)


# Register your models here.
admin.site.register(History, HistoryAdmin)
