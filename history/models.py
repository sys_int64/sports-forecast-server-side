from django.db import models
from django.db.models import Q

from express.models import Express
from player.models import Player
from forecast.models import Forecast
from datetime import datetime

import locale


class History(models.Model):
    class Meta:
        verbose_name = "History"
        verbose_name_plural = "History"

    forecast = models.ForeignKey(Forecast, verbose_name="Forecast", null=True, blank=True)
    express = models.ForeignKey(Express, verbose_name="Express", null=True, blank=True)
    datetime = models.DateTimeField(default=datetime.now)
    win = models.BooleanField("Win", default=True)
    price = models.FloatField("Bet", default="100")
    revert = models.BooleanField("Return bets", default=False)
    approved = models.BooleanField("Approved", default=False)

    def __str__(self):
        return self.forecast.__str__() if self.forecast else self.express.__str__()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.forecast:
            self.datetime = self.forecast.datetime
        if self.express:
            self.datetime = self.express.forecast_set.first().datetime

        super(History, self).save(force_insert, force_update, using, update_fields)

    def serialize(self, lang, short=False):
        result = {
            "id": self.pk,
            "win": self.win,
            "price": self.price,
        }

        item = None

        if self.forecast is not None:
            item = self.forecast.serialize(lang) if not short else self.forecast.pk

        if self.express is not None:
            item = self.express.serialize(lang) if not short else self.express.pk

        # if not isinstance(item, int):
        #     item.update({"express": self.express is not None})

        result["forecast"] = item
        result["express"] = self.express is not None

        return result

    def get_odd(self):
        if self.forecast:
            return self.forecast.odd

        if self.express:
            odd = 1.0

            for forecast in self.express.forecast_set.all():
                odd *= forecast.odd

            return odd

        return 0

    @staticmethod
    def format_total(num):
        locale.setlocale(locale.LC_ALL, '')

        if num > 10000:
            num = locale.format("%d", num, grouping=True)
            return num.replace(",", " ")

        return "{:d}".format(round(num))

    @staticmethod
    def calc_total(sport):
        query = ~Q(forecast=None) & Q(forecast__sport=sport)
        query |= ~Q(express=None)
        query &= Q(approved=True)
        query_set = History.objects.filter(query)
        total = 0

        for item in query_set:
            if item.win:
                total += abs(item.price* item.get_odd())
            else:
                total -= abs(item.price)

        return History.format_total(total)
