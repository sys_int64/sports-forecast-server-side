from django import forms
from django.contrib import admin

from express.models import Express, ExpressTranslation
from forecast.models import ForecastLink, Forecast
from languages.admin import SingleLanguageInline
from sports_forecast.forms import ExpressTranslationForm, ExpressForm


class TranslationInline(SingleLanguageInline):
    model = ExpressTranslation
    extra = 0
    form = ExpressTranslationForm
    max_num = 999


class ForecastLinkForm(forms.ModelForm):
    class Meta:
        model = ForecastLink
        fields = "__all__"


class ForecastsInline(admin.StackedInline):
    model = ForecastLink
    form = ForecastLinkForm
    extra = 1
    max_num = 5
    # form = make_ajax_form(ForecastLink, {
    #     "forecast": 'forecast'
    # })


class ExpressAdmin(admin.ModelAdmin):
    inlines = [TranslationInline, ForecastsInline]
    form = ExpressForm


admin.site.register(Express, ExpressAdmin)
