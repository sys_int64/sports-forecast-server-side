from django.db import models

from languages.models import Language


class Express(models.Model):
    free = models.BooleanField("Free", default=True)
    archive = models.BooleanField("Archive", default=False)

    def get_translation(self, lang):
        translations = self.expresstranslation_set.all()
        en_translation = None

        for translation in translations:
            if translation.language.pk == lang:
                return translation
            elif translation.language.pk == "us":
                en_translation = translation

        return en_translation

    def get_odd(self):
        odd = 1.0

        for forecast in self.forecast_set.all():
            odd *= forecast.odd

        return odd

    def serialize(self, lang="us", short=False):
        items = []
        translation = self.get_translation(lang)

        if translation is None:
            return {}

        for item in self.forecast_set.all():
            items.append(item.serialize(lang, short))

        return {
            "title": str(self),
            "odd": "%.1f" % round(self.get_odd(), 1),
            "forecasts": items
        }

    def __str__(self):
        return "Express #"+str(self.id)


class ExpressTranslation(models.Model):
    express = models.ForeignKey(Express)
    language = models.ForeignKey(Language, related_name="express_lang",
                                 verbose_name="Язык", null=True, blank=True)
    title = models.CharField("Название", max_length=100)
