import os
from datetime import datetime
from fabric.api import run, env, cd, roles, local


env.roledefs['stage'] = ['revengebet.com']
env.roledefs['production'] = ['revengebet.com']

env.current_datetime = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
env.key_filename = [os.path.join(os.environ['HOME'], '.ssh', 'revengebet.pem')]
env.shell = '/bin/bash -c'
env.user = 'ubuntu'


def stage_env():
    env.project_root = '/home/ubuntu/demo-env/revengebet'
    env.python = '/home/ubuntu/.virtualenvs/demo/bin/python'
    env.pip = '/home/ubuntu/.virtualenvs/demo/bin/pip'


def production_env():
    env.project_root = '/home/ubuntu/env/revengebet'
    env.python = '/home/ubuntu/.virtualenvs/revengebet/bin/python'
    env.pip = '/home/ubuntu/.virtualenvs/revengebet/bin/pip'
    env.db_dumps_path = '/home/ubuntu/env/db_dumps'
    env.site_dumps_path = '/home/ubuntu/env/site_dumps'
    env.env_path = '/home/ubuntu/env'


@roles('production')
def dump(warn_only=False):
    production_env()
    run(
        'cp {env_path}/db.sqlite3 {db_dumps_path}/{datetime}'.format(
            env_path=env.env_path,
            db_dumps_path=env.db_dumps_path,
            datetime=env.current_datetime
        ),
        warn_only=warn_only
    )
    run(
        'cp -r {env_path}/revengebet {site_dumps_path}/{datetime}'.format(
            env_path=env.env_path,
            site_dumps_path=env.site_dumps_path,
            datetime=env.current_datetime
        ),
        warn_only=warn_only
    )


@roles('production')
def rollback(date):
    production_env()
    dump(warn_only=True)

    run('echo "{datetime}" >> {env_path}/rollback_dumbs.txt'.format(
        env_path=env.env_path,
        datetime=env.current_datetime
    ))

    run('test -e {}/{}'.format(env.db_dumps_path, date))
    run('test -e {}/{}'.format(env.site_dumps_path, date))

    run('rm -rf {}/revengebet'.format(env.env_path))
    run('rm -rf {}/db.sqlite3'.format(env.env_path))

    run(
        'cp {db_dumps_path}/{date} {env_path}/db.sqlite3'.format(
            db_dumps_path=env.db_dumps_path,
            env_path=env.env_path,
            date=date
        )
    )
    run(
        'cp -r {site_dumps_path}/{date} {env_path}/revengebet'.format(
            site_dumps_path=env.site_dumps_path,
            env_path=env.env_path,
            date=date
        )
    )


@roles('production')
def clean_up_dumps(days=0):
    production_env()
    days = int(days)

    if days == 0:
        run('find %s/* -type f -exec rm -rf {} \;' % env.db_dumps_path, warn_only=True)
        run('find %s/* -type d -exec rm -rf {} \;' % env.site_dumps_path, warn_only=True)
    else:
        run('find %s/* -type f -ctime +%d -exec rm -rf {} \;' % (env.db_dumps_path, days), warn_only=True)
        run('find %s/* -type d -ctime +%d -exec rm -rf {} \;' % (env.site_dumps_path, days), warn_only=True)


@roles('stage', 'production')
def deploy(custom_branch=""):
    if 'stage' in env.roles:
        stage_env()
        branch = "develop"

        if custom_branch:
            branch = custom_branch
    elif 'production' in env.roles:
        production_env()
        branch = "master"
        dump()

        run('echo "{datetime}" >> {env_path}/releases_dumps.txt'.format(
            env_path=env.env_path,
            datetime=env.current_datetime
        ))
    else:
        return

    with cd(env.project_root):
        run('git pull origin '+branch)
        run(
            '{pip} install --upgrade -r {filepath}'.format(
                pip=env.pip,
                filepath=os.path.join(env.project_root, 'requirements.txt')
            )
        )
        run('{} manage.py collectstatic --noinput'.format(env.python))
        run('sh migrate.sh {}'.format(env.python))


@roles('stage', 'production')
def restart_supervisor():
    if 'stage' in env.roles:
        run("sudo supervisorctl stop demo")
        run("sudo supervisorctl stop demo-forecasts-watcher")

        run("pkill -9 -f \"manage.py runserver localhost:12346\"", warn_only=True)
        run("pkill -9 -f \"python /.*/demo-env/.*/manage.py checknewforecasts\"", warn_only=True)

        run("sudo supervisorctl start demo")
        run("sudo supervisorctl start demo-forecasts-watcher")

    if 'production' in env.roles:
        run("sudo supervisorctl stop revengebet")
        run("sudo supervisorctl stop tips-watcher")

        run('pkill -9 -f "manage.py runserver localhost:12346"', warn_only=True)
        run('pkill -9 -f "python /.*/env/.*/manage.py checknewforecasts"', warn_only=True)

        run("sudo supervisorctl start revengebet")
        run("sudo supervisorctl start tips-watcher")


@roles('production')
def display_rollback_dumps():
    production_env()
    run('cat {}/rollback_dumps.txt'.format(env.env_path))


@roles('production')
def display_releases_dumps():
    production_env()
    run('cat {}/releases_dumps.txt'.format(env.env_path))
