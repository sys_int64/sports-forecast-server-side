var win  = false;
var lose = false;
var draw = false;

(function($) {

    $(document).on('formset:added', function(event, $row, formsetName) {
        var win_text  = $(".languages-widget li.active a").attr("win");
        var draw_text = $(".languages-widget li.active a").attr("draw");
        var lang      = $(".languages-widget li.active a").attr("lang");

        var p1_text = $("#id_player_1_text").html();
        var p2_text = $("#id_player_2_text").html();

        if ($("#langs_player_1 ."+lang).length > 0)
            p1_text = $("#langs_player_1 ."+lang).attr("title");

        if ($("#langs_player_2 ."+lang).length > 0)
            p2_text = $("#langs_player_2 ."+lang).attr("title");

        var player1_text = $("#id_player_1_text").html();
        var player2_text = $("#id_player_2_text").html();

        win_text  = win_text .trim() == "" ? "will win this game"  : win_text;
        draw_text = draw_text.trim() == "" ? "will draw this game" : draw_text;

        if (win) {
            win = false;
            $($row).find(".vTextField").val(p1_text+" "+win_text);
        } else if (lose) {
            lose = false;
            $($row).find(".vTextField").val(p2_text+" "+win_text);
        } else if (draw) {
            draw = false;
            $($row).find(".vTextField").val(draw_text);
        }
    });

    $(document).ready(function() {
        $(".tip_win").click(function(e) {
            win = true;
            $(".add-row a").trigger("click");
            e.preventDefault();
        });

        $(".tip_lose").click(function(e) {
            lose = true;
            $(".add-row a").trigger("click");
            e.preventDefault();
        });

        $(".tip_draw").click(function(e) {
            draw = true;
            $(".add-row a").trigger("click");
            e.preventDefault();
        });

        $(".autocomplete_input").keydown(function(e) {
            $me = $(this);
            $ul = $(this).parent(".autocomplete_wrapper").children("ul");

            var keyCode = e.keyCode || e.which;

            if (keyCode == 9 || keyCode == 13) { // Tab
                $lia = $ul.find(".active a")
                $($lia.attr("href")).val($lia.html());

                if (keyCode == 13) {
                    $me.blur();
                    closeList();
                }

                e.preventDefault();
                return;
            }

            if (keyCode == 38) { // Up
                e.preventDefault();
                $actli = $ul.find(".active");

                if ($actli.is(":first-child")) {
                    $actli.removeClass("active");
                    $ul.find("li:last-child").addClass("active");
                    return;
                }

                $actli.removeClass("active");
                $actli.prev().addClass("active");

                return;
            }

            if (keyCode == 40) { // Down
                e.preventDefault();
                $actli = $ul.find(".active");

                if ($actli.is(":last-child")) {
                    $actli.removeClass("active");
                    $ul.find("li:first-child").addClass("active");
                    return;
                }

                $actli.removeClass("active");
                $actli.next().addClass("active");

                return;
            }

            $.getJSON("/api/tips.get/?offset=0&count=10&lang="+$(".languages-widget li.active a").attr("lang")+"&q="+$(this).val(), function(data) {
                var first = true;
                $ul.html("");
                $(data.tips).each(function() {
                    var res = $(this)[0];
                    console.log(res);
                    var cls = first ? "active" : "";
                    $ul.append("<li class='"+cls+"'><a href='#"+$me.attr("id")+"'>"+res+"</a></li>");
                    first = false;
                });
                checkChilds();
                updateTipsEvents();
            });

        });

        $(".autocomplete_input").blur(function() {
            $ul = $(this).parent(".autocomplete_wrapper").children("");
            $ul.html("");
            checkChilds();
        });
    });

    function closeList() {
        $(".autocomplete_wrapper ul").html("");
        checkChilds();
    }

    function checkChilds() {
        if ($(".autocomplete_wrapper ul").children().length <= 0)
            $(".autocomplete_wrapper ul").addClass("empty");
        else
            $(".autocomplete_wrapper ul").removeClass("empty");
    }

    function updateTipsEvents() {
        $(".autocomplete_wrapper li").mousedown(function(e) {
            $($(this).children("a").attr("href")).val($(this).children("a").html());
            $(".autocomplete_input").blur();
            e.preventDefault();
        });

        $(".autocomplete_wrapper li").mouseenter(function() {
            $(".autocomplete_wrapper li.active").removeClass("active");
            $(this).addClass("active");
        });
    }

})(django.jQuery);
