function checkActiveMenuItem() {
    $(".fixed-menu .anchor-link").removeClass("active");
    $("#home-link").addClass("active");
    var ids = ["tips", "tip-history", "how-it-work"];
    var body = $(window);
    var scrollTop = $(body).scrollTop();

    //console.log(topScroll);

    ids.forEach(function(item, i, arr) {
        var elTop  = $("#"+item).offset().top;
        var offset = $("#"+item).attr("offset");

        if (typeof offset !== typeof undefined && offset !== false) {
            elTop += parseInt(offset)-2;
        }

        if (scrollTop >= elTop) {
            $(".fixed-menu .anchor-link").removeClass("active");
            $("#"+item+"-link").addClass("active");
        }
    });
}

$(window).scroll(function () {
    checkActiveMenuItem();
});

$(document).ready(function() {
    $("input[type=checkbox]").change(function() {
        if ($(this).is(":checked")) {
            $(this).closest(".checkbox").addClass("checked");
        } else {
            $(this).closest(".checkbox").removeClass("checked");
        }

        $(this).closest("label").removeClass("has-error");
    });

    $("input[type=text], input[type=email], input[type=tel]").click(function() {
        $(this).removeClass("has-error");
    })

    checkActiveMenuItem();
    // function getPoints() {
    //     var points = [];
    //     var counter = 1;
    //
    //     $("#graph .point").each(function(){
    //         var value = parseInt($(this).attr("value"));
    //         var label = $(this).attr("label");
    //         var label_arr = label.split(".");
    //         //var date = new Date(parseInt(label_arr[2]), parseInt(label_arr[1]), parseInt(label_arr[0]));
    //         var date = new Date(parseInt(label_arr[2]), parseInt(label_arr[1])-1, parseInt(label_arr[0]));
    //
    //         ++counter;
    //         points.push({ x: date, y: value });
    //     });
    //
    //     return points;
    // }
    //
    // var chart = new CanvasJS.Chart($(".graph").attr('id'), {
    //     animationEnabled: true,
    //     axisX:{
    //         valueFormatString: "DD.MM",
    //         gridThickness: 0,
    //         tickLength: 0,
    //     },
    //
    //     axisY: {
    //         interval: 400,
    //         valueFormatString: "0$",
    //         gridThickness: 0,
    //         tickLength: 0,
    //         color: "#a8e032",
    //     },
    //
    //     data: [
    //         {
    //             type: "spline",
    //             lineThickness: 6,
    //             markerType: "circle",
    //             markerSize: 16,
    //             color: $(".graph").css('color'),
    //             dataPoints: getPoints()
    //         }
    //     ]
    // });
    //
    // chart.render();
    var body = $("html, body");

    // $(".canvasjs-chart-credit").css({'display': 'none'});

    $(".anchor-link").click(function(e) {
        var targetId = $(this).attr("href");
        var targetPos = 0;

        if (targetId != "#") {
            targetPos = $(targetId).offset().top;
            var offset = $(targetId).attr("offset");

            if (typeof offset !== typeof undefined && offset !== false) {
                targetPos += parseInt(offset);
            }
        }

        body.stop().animate({scrollTop: targetPos}, '500', 'swing');
        e.preventDefault();
    });

    $('#unlock-modal').on('shown.bs.modal', function () {
        $("#unlock-qr").html("");
        $("#unlock-modal .modal-body").hide();
        $("#unlock-modal .modal-error").hide();
        $("#unlock-modal .loading").show();

        $.post("/payment/unlock/", {
            "csrfmiddlewaretoken": $("input[name=csrfmiddlewaretoken]:first").val(),
            "sport": $('#unlock-modal').attr("sportid"),
        }).done(function(data) {
            $("#unlock-modal .loading").hide();

            if (data.error) {
                $("#unlock-modal .modal-error").show();
                $("#unlock-modal .modal-error").html(data.error);
                return false;
            }

            $("#unlock-modal .modal-body").show();
            $("#btc-address").attr("value", data.btc_address);
            $("#btc-price").html(data.btc_price)

            var qrcode = new QRCode(document.getElementById("unlock-qr"), {
                text: data.bitcoin_uri,
                width: 186,
                height: 186,
                colorDark : "#000000",
                colorLight : "#ffffff",
                correctLevel : QRCode.CorrectLevel.H
            });
        });
    });

});
