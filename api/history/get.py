"""
@api {get} /history.get/ История игр
@apiName HistoryGet
@apiGroup History

@apiDescription
    Возвращает список предыдущих игр. Для получения списка
    записей, небходимо указать начало выборки определенного подмножества
    записей - `offset` и количество записей, которое необходимо
    получить - `count`.

@apiParam {Number} offset смещение, необходимое для выборки определенного подмножества записей.
@apiParam {Number} count количество записей, которое необходимо получить (но не более 100).
@apiParam {Number} sport Вывести записи только одного вида спорта\
    &nbsp;&nbsp;&nbsp;<b>0</b> - Футбол\
    &nbsp;&nbsp;&nbsp;<b>1</b> - Баскетбол\
    &nbsp;&nbsp;&nbsp;<b>2</b> - Тенис\
    &nbsp;&nbsp;&nbsp;<b>3</b> - Волейбол

@apiSuccess {Number} id Уникальный идентификатор.
@apiSuccess {Boolean} win true, если выйграл
@apiSuccess {String} total Сумма
@apiSuccess {Object} forecast Модель <a href="#api-Forecast-ForecastGet">Forecast</a>.

@apiError {String} error Код ошибки
"""
from django.db.models import Q
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from django.conf import settings
from django.contrib.auth.models import User
from datetime import timedelta, datetime
from django.utils import timezone

from history.models import History
from dateutil.relativedelta import relativedelta
import locale


class HistoryGet(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    express = False

    def get(self, request, format=None):

        # print("Timestamp is "+str(1462896168694))
        # print(datetime.fromtimestamp(1462896168694/1000.0)+relativedelta(months=1))

        offset = request.GET.get("offset", 0)
        count = request.GET.get("count", 10)
        sport = request.GET.get("sport")
        lang = request.GET.get("lang", "us")
        short = request.GET.get("short") is not None

        # items = History.objects.filter(forecast__sport=sport, price__gt=0)

        if sport:
            items = History.objects.filter(forecast__sport=sport, approved=True)
        else:
            items = History.objects.filter(approved=True)

        if self.express:
            items = History.objects.filter(Q(approved=True) & ~Q(express=None))

        items = items.order_by("-datetime")
        items = items[int(offset):int(offset)+int(count)]
        items_arr = []

        # query = ~Q(forecast=None) & Q(forecast__sport=sport)
        # query |= ~Q(express=None)
        # # query &= Q(approved=True)
        #
        # all_history = History.objects.filter(query)
        # total = 0

        # for item in all_history:
        #     if item.win:
        #         total += abs(item.price*item.forecast.odd)
        #     else:
        #         total -= abs(item.price)
        total = History.calc_total(sport)

        for item in items:
            items_arr.append(item.serialize(lang, short))

        # locale.setlocale(locale.LC_ALL, '')

        # if total > 10000:
        #     total = locale.format("%d", total, grouping=True)
        #     # total = total.replace(",", " ")
        # else:
        #     total = "{:d}".format(round(total))

        return Response({"history": items_arr, "total": total})
