from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from django.conf import settings
from history.models import History
import locale
from datetime import datetime, timedelta, date


class RateGet(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    @staticmethod
    def rate(history, year):
        rate_set = []
        node = 0
        total = 0
        current_date = None
        added = False

        for item in history:
            added = False

            if current_date is None:
                current_date = item.datetime.date()
                date_1 = current_date-timedelta(days=1)

                if year:
                    label = str(date_1.day)+"."+"{:02d}".format(date_1.month)+"."+str(date_1.year)
                else:
                    label = str(date_1.day)+"."+"{:02d}".format(date_1.month)

                rate_set.append({
                    "val": 100.0,
                    "label": label,
                })

            if item.win:
                node += abs(item.price*item.get_odd())
            else:
                node -= abs(item.price)

            #total += node
            if item.datetime.date() != current_date:
                added = True

                if (year):
                    label = str(current_date.day)+"."+"{:02d}".format(current_date.month)+"."+str(current_date.year)
                else:
                    label = str(current_date.day)+"."+"{:02d}".format(current_date.month)

                current_date = item.datetime.date()
                rate_set.append({
                    "val": node,
                    "label": label,
                })

                total += node
                #node = 0

        if not added and current_date is not None:
            if (year):
                label = str(current_date.day)+"."+"{:02d}".format(current_date.month)+"."+str(current_date.year)
            else:
                label = str(current_date.day)+"."+"{:02d}".format(current_date.month)

            rate_set.append({
                "val": node,
                "label": label,
            })

        total = node
        # locale.setlocale(locale.LC_ALL, '')
        #
        # if total > 10000:
        #     total = locale.format("%d", total, grouping=True)
        #     # total = total.replace(",", " ")
        # else:
        #     total = "{:d}".format(round(total))
        total = 0

        return {"rate_set": rate_set, "total": total}

    def get(self, request, format=None):
        sport = request.GET.get("sport")

        if sport:
            history = History.objects.filter(
                forecast__sport=sport,
                approved=True
            ).order_by("datetime")
        else:
            history = History.objects.filter(approved=True).order_by("datetime")

        return Response(RateGet.rate(history, False))
