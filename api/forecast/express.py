from datetime import datetime, timedelta
from django.utils import timezone

from rest_framework.permissions import IsAuthenticatedOrReadOnly
from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework.views import APIView

from express.models import Express
from history.models import History


def move_archive_expired():
    time_threshold = timezone.now()+timedelta(hours=4)
    expresses = Express.objects.filter(archive=False)

    for express in expresses.all():
        archive = True

        for forecast in express.forecast_set.all():
            archive = archive and forecast.datetime < time_threshold

        express.archive = archive
        express.save()

        if express.archive:
            History.objects.create(express=express)


class ForecastGetExpress(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request):
        offset = request.GET.get("offset", 0)
        count = request.GET.get("count", 10)
        device_id = "device_" + request.GET.get("device_id", "")
        item_id = request.GET.get("id")
        lang = request.GET.get("lang", "us")
        short = request.GET.get("short") is not None
        timestamp = request.GET.get("timestamp", None)

        move_archive_expired()

        try:
            user = User.objects.get(username=device_id)
        except User.DoesNotExist:
            return Response({"error": "U0"})

        if item_id is not None:
            try:
                item = Express.objects.get(pk=int(item_id))
            except Express.DoesNotExist:
                return Response({"error": "E0"})

            return Response({"express": item.serialize(lang, short)})
        else:
            expresses, subscribe = user.userprofile.get_expresses(timestamp)
            items = expresses.all()
            items = items[int(offset):int(offset) + int(count)]
            items_arr = []

            for item in items:
                items_arr.append(item.serialize(lang, short))

            return Response({"expresses": items_arr})
