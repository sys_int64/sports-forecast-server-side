from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.dateparse import parse_datetime
from django.utils.timezone import FixedOffset, LocalTimezone
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.views import APIView
from dateutil import parser

from express.models import Express
from forecast.models import Forecast


class ForecastCount(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, format=None):
        device_id = "device_" + request.GET.get("device_id", "")

        try:
            user = User.objects.get(username=device_id)
        except User.DoesNotExist:
            return Response({"error": "U0"})

        profile = user.userprofile

        # football, _ = profile.get_forecasts(Forecast.SPORT_FOOTBALL)
        # basketball, _ = profile.get_forecasts(Forecast.SPORT_BASKETBALL)
        # tennis, _ = profile.get_forecasts(Forecast.SPORT_TENNIS)
        # volleyball, _ = profile.get_forecasts(Forecast.SPORT_VOLLEYBALL)
        football = Forecast.objects.filter(sport=Forecast.SPORT_FOOTBALL, archive=False)
        basketball = Forecast.objects.filter(sport=Forecast.SPORT_BASKETBALL, archive=False)
        tennis = Forecast.objects.filter(sport=Forecast.SPORT_TENNIS, archive=False)
        hockey = Forecast.objects.filter(sport=Forecast.SPORT_HOCKEY, archive=False)
        express = Express.objects.filter(archive=False)

        football_new = football.filter(datetime_create__gte=profile.last_datetime_football)
        basketball_new = basketball.filter(datetime_create__gte=profile.last_datetime_basketball)
        tennis_new = tennis.filter(datetime_create__gte=profile.last_datetime_tennis)
        hockey_new = hockey.filter(datetime_create__gte=profile.last_datetime_volleyball)
        #express_new = hockey.filter(datetime_create__gte=profile.last_datetime_volleyball)

        return Response({
            "football": {
                "total": football.count(),
                "new": football_new.count(),
            },
            "basketball": {
                "total": basketball.count(),
                "new": basketball_new.count(),
            },
            "tennis": {
                "total": tennis.count(),
                "new": tennis_new.count(),
            },
            "hockey": {
                "total": hockey.count(),
                "new": hockey_new.count(),
            },
            "volleyball": {
                "total": hockey.count(),
                "new": hockey_new.count(),
            },
            "express": {
                "total": express.count(),
                "new": 0,  # TODO: Implement
            }
        })
        # datetime = request.GET.get("datetime")
        # datetime = parser.parse(datetime)
        # # (year, month = None, day = None, hour = 0, minute = 0, second = 0, microsecond = 0, tzinfo = None
        # # datetime = timezone.datetime(2016, 8, 14, 14, 16, 0, 0, tzinfo=LocalTimezone())
        # print(datetime)

        # return Response({"forecasts": datetime})
