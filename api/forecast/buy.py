"""
@api {post} /forecast.buy/ Купить пакет подсказок
@apiName ForecastBuy
@apiGroup Forecast

@apiDescription
    Осуществляется покупка пакета подсказок по `packet_id` для пользователя
    с устройством `device_id`.

@apiParam {Number} device_id Уникальный идентификатор прогноза.
@apiParam {Number} packet_id Уникальный идентификатор пакета.

@apiSuccess {Number} ok true, если покупка соверешена успешно.
@apiError {String} error Код ошибки
"""

from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Q

from forecast.models import Forecast
from history.models import History
from datetime import timedelta, datetime
from django.utils import timezone
from tariffs.models import TariffsTips


class ForecastBuy(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        device_id = "device_"+request.POST.get("device_id")
        packet_id = request.POST.get("packet_id")

        try:
            packet = TariffsTips.objects.get(pk=packet_id)
        except TariffsTips.DoesNotExist:
            return Response({"error": "F1"})

        try:
            user = User.objects.get(username=device_id)
        except User.DoesNotExist:
            return Response({"error": "U0"})

        user.userprofile.left_tips += int(packet.tips)
        user.userprofile.save()

        return Response({"ok": True})
