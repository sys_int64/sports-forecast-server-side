"""
@api {post} /forecast.open/ Разблокировать подсказку
@apiName ForecastOpen
@apiGroup Forecast

@apiDescription
    Открывает заблокированную подсказку.

@apiParam {Number} device_id Уникальный идентификатор прогноза.
@apiParam {Number} id Уникальный идентификатор прогноза.
@apiParam {String} lang Язык

@apiSuccess {Number} ok true, если покупка соверешена успешно.
@apiError {String} error Код ошибки
"""

from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Q

from forecast.models import Forecast
from history.models import History
from datetime import timedelta, datetime
from django.utils import timezone


class ForecastOpen(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        device_id = "device_"+request.POST.get("device_id")
        item_id = request.POST.get("id")
        lang = request.POST.get("lang")

        try:
            user = User.objects.get(username=device_id)
        except User.DoesNotExist:
            return Response({"error": "U0"})

        try:
            forecast = Forecast.objects.get(pk=int(item_id))
        except Forecast.DoesNotExist:
            return Response({"error": "F0"})

        if (user.userprofile.left_tips <= 0):
            user.userprofile.left_tips = 0
            user.userprofile.save()
            return Response({"error": "U2"})

        if (user.userprofile.has_subscribe(forecast.sport)):
            return Response({"error": "U4"})

        if (forecast in user.userprofile.viewed_forecasts.all()):
            return Response({"error": "U3"})

        user.userprofile.left_tips -= 1
        user.userprofile.viewed_forecasts.add(forecast)
        user.userprofile.save()

        return Response({"forecast": forecast.serialize(lang)})
