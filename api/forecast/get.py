"""
@api {get} /forecast.get/ Список прогнозов
@apiName ForecastGet
@apiGroup Forecast

@apiDescription
    Возвращает список прогнозов либо один прогноз. Для получения списка
    записей, небходимо указать начало выборки определенного подмножества
    записей - `offset` и количество записей, которое необходимо
    получить - `count`. Чтобы получить одну конкретную запись, необходимо
    указать уникальный идентификатор записи - `id`.\
    Если заполнено поле `sport`, метод вернет только записи, принадлежащие
    определенному виду спорта, которое записано в `sport`.

@apiParam {Number} id Уникальный идентификатор прогноза.
@apiParam {Number} offset Смещение, необходимое для выборки определенного подмножества записей.
@apiParam {Number} count Количество записей, которое необходимо получить (но не более 100).
@apiParam {Param}  short Если есть, то home, away - возвращают id
@apiParam {Number} sport Вывести записи только одного вида спорта\
    &nbsp;&nbsp;&nbsp;<b>0</b> - Футбол\
    &nbsp;&nbsp;&nbsp;<b>1</b> - Баскетбол\
    &nbsp;&nbsp;&nbsp;<b>2</b> - Тенис\
    &nbsp;&nbsp;&nbsp;<b>3</b> - Волейбол

@apiSuccess {Number} id Уникальный идентификатор прогноза.
@apiSuccess {String} text Прогноз
@apiSuccess {Number} odd Коэфициент
@apiSuccess {String} sport Вид спорта\
    &nbsp;&nbsp;&nbsp;<b>0</b> - Футбол\
    &nbsp;&nbsp;&nbsp;<b>1</b> - Баскетбол\
    &nbsp;&nbsp;&nbsp;<b>2</b> - Тенис\
    &nbsp;&nbsp;&nbsp;<b>3</b> - Волейбол
@apiSuccess {String} date Дата
@apiSuccess {String} time Время
@apiSuccess {String} home Первая команда, запись <a href="#api-Player-PlayerGet">Player</a>.
@apiSuccess {String} away Вторая команда.
@apiSuccess {[Tip]}  tips Массив с <a href="#api-Tip-TipGet">подсказками</a>
@apiSuccess {Object} next Следующий платный прогноз

@apiError {String} error Код ошибки

@apiSuccessExample Пример
{
    "next": {
        "odd": 4.0,
        "tips": 2,
        "id": 19
    },
    "forecast": {
        "sport": "0",
        "time": "10:47",
        "date": "Февраль 10:47",
        "tips": [
            {
                "text": "ASDASDasd\r\nasdasd",
                "id": 1,
                "forecast_id": 1
            },
            {
                "text": "asdasdaf\r\nsdfsdf",
                "id": 2,
                "forecast_id": 1
            }
        ],
        "id": 1,
        "home": {
            "title": "Test",
            "id": 1,
            "text": "Test",
            "image": "https://pure-gorge-94712.herokuapp.com/media/icon1-01.png"
        },
        "away": {
            "title": "Test 2",
            "id": 2,
            "text": "Test 2\r\nTest 2",
            "image": "https://pure-gorge-94712.herokuapp.com/media/rouble.png"
        },
        "odd": 1.0
    }
}
"""

from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Q

from forecast.models import Forecast
from history.models import History
from datetime import timedelta, datetime
from django.utils import timezone
from languages.models import Languages


def move_archive_expired():
    time_threshold = datetime.now()+timedelta(hours=4)
    forecasts_query = Forecast.objects.filter(datetime__lt=time_threshold,
                                              archive=False, express=None)

    for forecast in forecasts_query.all():
        forecast.archive = True
        forecast.save()
        History.objects.create(forecast=forecast)


class ForecastInactive(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request):
        inactive = []
        f_settings = Languages.objects.get()

        if not f_settings.football_enabled:
            inactive.append(Forecast.SPORT_FOOTBALL)

        if not f_settings.basketball_enabled:
            inactive.append(Forecast.SPORT_BASKETBALL)

        if not f_settings.tennis_enabled:
            inactive.append(Forecast.SPORT_TENNIS)

        if not f_settings.hockey_enabled:
            inactive.append(Forecast.SPORT_HOCKEY)

        return Response(inactive)


class ForecastGet(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, format=None):

        f_settings = Languages.objects.get()
        sport = request.GET.get("sport")

        if sport == Forecast.SPORT_FOOTBALL and not f_settings.football_enabled:
            return Response(None, status.HTTP_204_NO_CONTENT)

        if sport == Forecast.SPORT_BASKETBALL and not f_settings.basketball_enabled:
            return Response(None, status.HTTP_204_NO_CONTENT)

        if sport == Forecast.SPORT_TENNIS and not f_settings.tennis_enabled:
            return Response(None, status.HTTP_204_NO_CONTENT)

        if sport == Forecast.SPORT_HOCKEY and not f_settings.hockey_enabled:
            return Response(None, status.HTTP_204_NO_CONTENT)

        offset = request.GET.get("offset")
        count = request.GET.get("count")
        device_id = "device_"+request.GET.get("device_id", "")
        item_id = request.GET.get("id")
        lang = request.GET.get("lang")
        short = request.GET.get("short")
        timestamp = request.GET.get("timestamp", None)
        move_archive_expired()  # TODO: move to managment command and run in supervisor
        short = short is not None

        try:
            user = User.objects.get(username=device_id)
        except User.DoesNotExist:
            return Response({"error": "U0"})

        user.userprofile.update_last_datetime(sport)

        if item_id is not None:

            try:
                item = Forecast.objects.get(pk=int(item_id))
            except Forecast.DoesNotExist:
                return Response({"error": "F0"})

            return Response({"forecast": item.serialize(lang, short)})
        else:
            forecasts, subscribe = user.userprofile.get_forecasts(sport, timestamp)
            items = forecasts.order_by("datetime").all()
            items = items[int(offset):int(offset)+int(count)]
            items_arr = []

            for item in items:
                items_arr.append(item.serialize(lang, short))

            forecasts = user.userprofile.get_free_forecasts(sport)

            if len(forecasts) >= 1 and not subscribe:
                next_payable = {
                    "odd": forecasts[0].odd,
                    "tips": len(forecasts[0].tip_set.all()),
                    "id": forecasts[0].pk,
                }
            else:
                next_payable = None

            return Response({"forecasts": items_arr, "next": next_payable})
