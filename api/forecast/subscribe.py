"""
@api {post} /forecast.subscribe/ Оформить подписку
@apiName ForecastSubscribe
@apiGroup Forecast

@apiDescription
    Оформить подписку на подсказки к спорту.

@apiParam {Number} device_id Уникальный идентификатор прогноза.
@apiParam {Number} sport Спорт, на который подписаться.

@apiSuccess {Number} ok true, если покупка соверешена успешно.
@apiError {String} error Код ошибки
"""

from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Q

from forecast.models import Forecast
from history.models import History
from datetime import timedelta, datetime, date
from django.utils import timezone
from dateutil.relativedelta import relativedelta


class ForecastSubscribe(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        device_id = "device_"+request.POST.get("device_id")
        sport = request.POST.get("sport")

        try:
            user = User.objects.get(username=device_id)
        except User.DoesNotExist:
            return Response({"error": "U0"})

        if (user.userprofile.has_subscribe(sport)):
            return Response({"error": "U4"})

        if (sport == Forecast.SPORT_FOOTBALL):
            now = user.userprofile.subscribe_football

            if (now < datetime.now().date()):
                now = datetime.now().date()

            d = now+timedelta(days=1)
            d += relativedelta(months=1)
            user.userprofile.subscribe_football = d

        elif (sport == Forecast.SPORT_BASKETBALL):
            now = user.userprofile.subscribe_basketball

            if (now < datetime.now().date()):
                now = datetime.now().date()

            d = now+timedelta(days=1)
            d += relativedelta(months=1)
            user.userprofile.subscribe_basketball = d

        elif (sport == Forecast.SPORT_TENNIS):
            now = user.userprofile.subscribe_tennis

            if (now < datetime.now().date()):
                now = datetime.now().date()

            d = now+timedelta(days=1)
            d += relativedelta(months=1)
            user.userprofile.subscribe_tennis = d

        elif (sport == Forecast.SPORT_VOLLEYBALL):
            now = user.userprofile.subscribe_volleyball

            if (now < datetime.now().date()):
                now = datetime.now().date()

            d = now+timedelta(days=1)
            d += relativedelta(months=1)
            user.userprofile.subscribe_volleyball = d

        user.userprofile.save()
        return Response({"ok": True})
