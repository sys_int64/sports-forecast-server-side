"""

"""

from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Q

from forecast.models import Forecast
from history.models import History
from datetime import timedelta, datetime
from django.utils import timezone


class ForecastNext(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, format=None):
        device_id = "device_"+request.GET.get("device_id")
        item_id = request.GET.get("id")
        sport = request.GET.get("sport")

        try:
            user = User.objects.get(username=device_id)
        except User.DoesNotExist:
            return Response({"error": "U0"})

        viewed_list = User.objects.filter(username=device_id).values_list('userprofile__viewed_forecasts__id', flat=True)

        if (viewed_list[0] is None):
            viewed_list = []

        query = Q(sport=sport, archive=False, free=False)
        query &= ~Q(id__in=viewed_list)
        objects = Forecast.objects.filter(query)

        if (len(objects) == 1 and not user.userprofile.has_subscribe(sport)):
            next_payable = {
                "odd": objects[0].odd,
                "tips": len(objects[0].tip_set.all()),
                "id": objects[0].pk,
            }
        else:
            next_payable = None

        return Response({"next": next_payable})
