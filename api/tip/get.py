"""
@api {get} /tip.get/ Список подсказок
@apiName TipGet
@apiGroup Tip

@apiDescription
    Возвращает список подсказок либо одну подсказку. Для получения списка
    записей, небходимо указать начало выборки определенного подмножества
    записей - `offset` и количество записей, которое необходимо
    получить - `count`. Чтобы получить одну конкретную запись, необходимо
    указать уникальный идентификатор записи - `id`

@apiParam {Number} id Уникальный идентификатор подсказки.
@apiParam {Number} offset смещение, необходимое для выборки определенного подмножества записей.
@apiParam {Number} count количество записей, которое необходимо получить (но не более 100).

@apiSuccess {Number} id Уникальный идентификатор подсказки.
@apiSuccess {String} forecast_id Уникальный идентификатор прогноза, к которому привязана подсказка
@apiSuccess {String} text Текст подсказки

@apiError {String} error Код ошибки

@apiSuccessExample Пример
{
    "tip": {
        "forecast_id": 1,
        "id": 1,
        "text": "ASDASDasd\r\nasdasd"
    }
}
"""

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from django.conf import settings
from django.contrib.auth.models import User

from forecast.models import Tip


class TipGet(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    def get(self, request, format=None):
        if not request.user.is_staff:
            return Response({"error": "UP"})

        offset = request.GET.get("offset")
        count = request.GET.get("count")
        lang = request.GET.get("lang")
        sport = request.GET.get("sport")
        q = request.GET.get("q")

        if q.strip() == "":
            return Response({"tips": []})

        items = Tip.objects.filter(text__icontains=q, language=lang) \
            .values_list("text").distinct()[:int(count)]

        return Response({"tips": items})
