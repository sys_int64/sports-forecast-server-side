"""
@api {get} /player.get/ Список команд
@apiName PlayerGet
@apiGroup Player

@apiDescription
    Возвращает список команд либо одина команда. Для получения списка
    записей, небходимо указать начало выборки определенного подмножества
    записей - `offset` и количество записей, которое необходимо
    получить - `count`. Чтобы получить одну конкретную запись, необходимо
    указать уникальный идентификатор записи - `id`

@apiParam {Number} id Уникальный идентификатор команды.
@apiParam {Number} offset смещение, необходимое для выборки определенного подмножества записей.
@apiParam {Number} count количество записей, которое необходимо получить (но не более 100).

@apiSuccess {Number} id Уникальный идентификатор команды.
@apiSuccess {String} title Название команды
@apiSuccess {String} text Описание команды
@apiSuccess {String} image Герб команды размера 200x200px
@apiSuccess {String} thumb Герб команды размера 96x96px

@apiError {String} error Код ошибки

@apiSuccessExample Пример
{
    "player": {
        "id": 2,
        "title": "Test 2",
        "text": "Test 2\r\nTest 2",
        "image": "https://pure-gorge-94712.herokuapp.com/media/rouble.png"
        "thumb": "..."
    }
}
"""

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from django.conf import settings
from django.contrib.auth.models import User

from player.models import Player


class PlayerGet(APIView):


    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    def get(self, request, format=None):
        offset = request.GET.get("offset")
        count = request.GET.get("count")
        item_id = request.GET.get("id")
        lang = request.GET.get("lang")
        translations = request.GET.get("translations")

        if (item_id is not None):

            try:
                item = Player.objects.get(pk=int(item_id))
            except Player.DoesNotExist:
                return Response({"error": "P0"})

            items_arr = []
            ctx = {
                "player": item.serialize(lang),
            }

            if (translations is not None):
                ctx.update(item.serialize_translations())

            return Response(ctx)
        else:
            items = Player.objects.all()[int(offset):int(offset)+int(count)]
            items_arr = []

            for item in items:
                items_arr.append(item.serialize(lang))

            return Response({"players": items_arr})
