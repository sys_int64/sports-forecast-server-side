#from rest_framework import routers
from django.conf.urls import url, include

from api.auth.compare_server import CompareServerView
from api.auth.login import LoginView
from api.auth.gcm import UpdateGCMTokenView
from api.forecast.count import ForecastCount
from api.forecast.express import ForecastGetExpress

from api.forecast.get import ForecastGet, ForecastInactive
from api.forecast.buy import ForecastBuy
from api.forecast.open import ForecastOpen
from api.forecast.next import ForecastNext
from api.forecast.subscribe import ForecastSubscribe
from api.player.get import PlayerGet
from api.tip.get import TipGet
from api.history.get import HistoryGet
from api.rate.get import RateGet
from api.tariffs.get import TariffsGet

# from api.user.details import UserMyView
# from api.user.register import UserRegisterView
# from api.user.set_gcm_token import SetGCMTokenView


urlpatterns = [

    # url (r'^user/my/$', UserMyView.as_view(), name='auth')
    # url (r'^user/register/$', UserRegisterView.as_view(), name='auth')

    # Authorithation

    url(r'^device\.auth/$', LoginView.as_view(), name='auth'),
    url(r'^device\.update-gcm/$', UpdateGCMTokenView.as_view(), name='auth'),
    url(r'^device\.compare-server/$', CompareServerView.as_view(), name='auth'),
    # url(r'^device\.auth/$', CustomTokenView.as_view(), name='auth'),
    # url(r'^device\.register/$', RegisterView.as_view(), name='auth'),
    # url(r'^device\.set-gcm-token/$', SetGCMTokenView.as_view(), name='auth'),

    # Forecast

    url(r'^sports\.inactive/$', ForecastInactive.as_view(), name='forecast'),
    url(r'^forecast\.get/$', ForecastGet.as_view(), name='forecast'),
    url(r'^forecast\.get\.express/$', ForecastGetExpress.as_view(), name='forecast'),
    url(r'^forecast\.count/$', ForecastCount.as_view(), name='forecast'),
    url(r'^forecast\.buy/$', ForecastBuy.as_view(), name='forecast'),
    url(r'^forecast\.open/$', ForecastOpen.as_view(), name='forecast'),
    url(r'^forecast\.next/$', ForecastNext.as_view(), name='forecast'),
    url(r'^forecast\.subscribe/$', ForecastSubscribe.as_view(), name='forecast'),
    url(r'^player\.get/$', PlayerGet.as_view(), name='player'),
    url(r'^tips\.get/$', TipGet.as_view(), name='tip'),
    url(r'^history\.get/$', HistoryGet.as_view(), name='history'),
    url(r'^history\.get\.express/$', HistoryGet.as_view(express=True), name='history'),
    url(r'^rate\.get/$', RateGet.as_view(), name='rate'),
    url(r'^tariffs\.get/$', TariffsGet.as_view(), name='tariffs'),
    # url(r'^rate\.sum/$', SumGet.as_view(), name='tip'),

]
