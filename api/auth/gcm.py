"""

@api {post} /device.update-gcm/ Установить gcm токен
@apiName Auth
@apiGroup Device
@apiDescription

Устанавливает/обновляет GCM токен для устройства `device_id`.

@apiParam {String} client_id Уникальный номер клиента
@apiParam {String} client_secret Секретный ключ клиента
@apiParam {String} device_id Идентификатор устройства
@apiParam {String} gcm_token GCM токен

@apiSuccess {String} ok True, если ошибок нет
@apiError   {String} error Код ошибки

"""

from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from django.conf import settings
from django.contrib.auth.models import User
from userprofile.validate import validate_client
from push_notifications.models import APNSDevice
import string
import random


class UpdateGCMTokenView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        device_id = request.POST.get("device_id")
        apns_token = request.POST.get("apns_token")
        client_id = request.POST.get("client_id")
        client_secret = request.POST.get("client_secret")
        gcm_token = request.POST.get("gcm_token")

        if not validate_client(client_id, client_secret):
            return Response({"error": "U1"})

        try:
            user = User.objects.get(username="device_"+device_id)
        except User.DoesNotExist:
            return Response({"error": "U0"})

        user.userprofile.gcm_token = gcm_token
        user.userprofile.save()

        return Response({"ok": True})
