from braces.views import LoginRequiredMixin
from django.views.generic import UpdateView
from oauth2_provider.exceptions import OAuthToolkitError
from oauth2_provider.http import HttpResponseUriRedirect
from oauth2_provider.models import AccessToken
from oauth2_provider.settings import oauth2_settings

from oauth2_provider.views import TokenView
from django.utils.decorators import method_decorator
from django.views.decorators.debug import sensitive_post_parameters
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import User

import json


class CustomTokenView (TokenView):

    @method_decorator(sensitive_post_parameters('password'))
    def post(self, request, *args, **kwargs):

        try:
            User.objects.get(username=request.POST.get("username"))
        except User.DoesNotExist:
            return JsonResponse({"error": "U0"})

        url, headers, body, status = self.create_token_response(request)

        if (status == 200):
            body = json.loads(body)
            token = AccessToken.objects.get(token=body["access_token"])
            body["user"] = token.user.userprofile.serialize()
            body = json.dumps(body)

        response = HttpResponse(content=body, status=status)

        for k, v in headers.items():
            response[k] = v

        return response
