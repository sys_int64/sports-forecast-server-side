

from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from django.conf import settings
from django.contrib.auth.models import User
from userprofile.validate import validate_client
import string
import random


class RegisterView(APIView):

    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        client_id = request.POST.get("client_id")
        client_secret = request.POST.get("client_secret")

        if (not validate_client(client_id, client_secret)):
            return Response({"error": "U2"})

        device_id = request.POST.get("device_id")
        chars = string.ascii_letters+string.digits
        secret = ''.join(random.choice(chars) for _ in range(128))

        if (device_id is None or device_id.strip() == ""):
            return Response({"error": "U1"})

        try:
            user = User.objects.get(username=device_id)
            user.set_password(secret)
            user.save()

            return Response({"secret_key": secret})
        except User.DoesNotExist:
            pass

        user = User.objects.create_user(username=device_id, password=secret)
        return Response({"secret_key": secret})
