"""

@api {post} /device.auth/ Авторизация устройства
@apiName Auth
@apiGroup Device
@apiDescription

Авторизация устройства, либо регистрация, если `device_id` не найден.

@apiParam {String} client_id Уникальный номер клиента
@apiParam {String} client_secret Секретный ключ клиента
@apiParam {String} device_id Идентификатор устройства
@apiParam {String} apns_token APNS токен

@apiSuccess {String} ok True, если ошибок нет
@apiError   {String} error Код ошибки

"""

from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from django.conf import settings
from django.contrib.auth.models import User

from forecast.models import Forecast
from userprofile.validate import validate_client
from push_notifications.models import APNSDevice
import string
import random
import pprint


def id_generator(size=6, chars=string.ascii_letters + string.digits + string.punctuation):
    return ''.join(random.choice(chars) for _ in range(size))


class LoginView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        device_id = request.POST.get("device_id")
        apns_token = request.POST.get("apns_token")
        gcm_token = request.POST.get("gcm_token")
        client_id = request.POST.get("client_id")
        client_secret = request.POST.get("client_secret")

        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(request.POST)

        # if not validate_client(client_id, client_secret):
        #    return Response({"error": "U1"})

        try:
            user = User.objects.get(username="device_" + device_id)
        except User.DoesNotExist:
            user = User.objects.create_user(username="device_" + device_id)

        if apns_token is not None:
            device_id = device_id.replace("-", "").lower()
            apns_token = apns_token.replace(" ", "")
            user.userprofile.apns_token = apns_token

            # try:
            APNSDevice.objects.filter(registration_id=apns_token).delete()
            # device.device_id = device_id
            # device.save()
            try:
                device = APNSDevice.objects.get(device_id=device_id)
                device.registration_id = apns_token
                device.save()
            except APNSDevice.DoesNotExist:
                device = APNSDevice()
                device.device_id = device_id
                device.registration_id = apns_token
                device.save()

        if gcm_token is not None:
            user.userprofile.gcm_token = gcm_token

        user.userprofile.is_device = True

        if user.userprofile.payload == "":
            user.userprofile.payload = id_generator(32)

        user.userprofile.save()

        max_count = -1
        start_sport = Forecast.SPORT_FOOTBALL

        for sport in Forecast.SPORTS_RANGE:
            count = Forecast.objects.filter(sport=sport, archive=False).count()

            if count > max_count:
                max_count = count
                start_sport = sport

        return Response({
            "user_id": user.id,
            "profile_id": user.userprofile.id,
            "secret_payload": user.userprofile.payload,
            "start_sport": start_sport
        })
