"""
@api {get} /tariffs.get/ Список тарифов
@apiName TariffsGet
@apiGroup Tariffs

@apiDescription
    Возвращает список доступных тарифов. Также показывает количство
    оставшихся подсказок

@apiSuccess {Number} id Уникальный идентификатор подсказки.
@apiSuccess {Number} left_tips Оставшиеся подсказки у пользователя
@apiSuccess {String} title Наименование пакета/подписки
@apiSuccess {Number} tips Количество подсказок в пакете
@apiSuccess {Number} price Цена пакета/подписки

@apiError {String} error Код ошибки

@apiSuccessExample Пример

{
    "left_tips": 2,
    "tariffs": {
        "packets": [
            {
                "id": 1,
                "tips": 10,
                "price": 2.0,
                "title": "Novice"
            },
            {
                "id": 2,
                "tips": 20,
                "price": 4.0,
                "title": "Middle"
            },
            {
                "id": 3,
                "tips": 40,
                "price": 6.0,
                "title": "Pro"
            }
        ],
        "subscribes": [
            {
                "id": 1,
                "price": 10.0,
                "title": "Football"
            },
            {
                "id": 2,
                "price": 20.0,
                "title": "Basketball"
            },
            {
                "id": 3,
                "price": 30.0,
                "title": "Tennis"
            },
            {
                "id": 4,
                "price": 40.0,
                "title": "Volleyball"
            }
        ]
    }
}
"""


from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.views import APIView

from django.conf import settings
from tariffs.models import Tariffs
from django.contrib.auth.models import User


class TariffsGet(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, format=None):
        device_id = "device_"+request.GET.get("device_id")
        tariffs = Tariffs.objects.get()

        try:
            user = User.objects.get(username=device_id)
        except User.DoesNotExist:
            return Response({"error": "U0"})

        return Response({
            "left_tips": user.userprofile.left_tips,
            "tariffs": tariffs.serialize()
        })
