from django.contrib import admin
from forecast.models import Forecast, Tip
from forecast.forms import ForecastForm, TipForm
from languages.admin import TabularLanguageInline
from django.db.models.query_utils import Q
from userprofile.models import UserProfile


class TipsInline(TabularLanguageInline):
    model = Tip
    extra = 0
    max_num = 99
    form = TipForm
    template = "admin/widgets/tip_inline.html"


class ForecastAdmin(admin.ModelAdmin):
    form = ForecastForm
    list_display = ('player_1', 'player_2', 'datetime', 'odd')
    list_filter = ['datetime', 'sport']
    raw_id_fields = ('player_1', 'player_2')
    search_fields = [
        'player_1__playertranslation__title',
        'player_2__playertranslation__title',
        'player_1__playertranslation__text',
        'player_2__playertranslation__text',
        'tip__text',
    ]

    inlines = [TipsInline]

    def get_queryset(self, request):
        qs = super(ForecastAdmin, self).get_queryset(request)
        perm_query = UserProfile.get_query_sportperm(request)
        return qs.filter(Q(archive=False, express=None) & perm_query)


admin.site.register(Forecast, ForecastAdmin)
