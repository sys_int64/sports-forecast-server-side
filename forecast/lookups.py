from django.db.models import Q

from forecast.models import Forecast
from ajax_select import LookupChannel, register


@register("forecast")
class ForecastLookup(LookupChannel):
    model = Forecast

    def get_query(self, q, request):
        query = Q(player_1__playertranslation__title__icontains=q)
        query |= Q(player_2__playertranslation__title__icontains=q)

        return self.model.objects.filter(query)[:10]

    def can_add(self, user, model):
        return True

    def format_item_display(self, item):
        return u"<span class='tag'>%s</span>" % item.name
