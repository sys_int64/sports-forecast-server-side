from django import forms
from forecast.models import Forecast, Tip
from string import Template
from django.utils.safestring import mark_safe
from player.models import Player
from django.template import loader
from languages.forms import LanguagesSelectInput, LanguageInput
from languages.models import Language
from datetime import datetime
from django.utils.html import format_html, format_html_join, smart_urlquote
from django.utils.translation import ugettext as _


class AdminDateWidget(forms.DateInput):
    @property
    def media(self):
        js = ["calendar.js", "admin/DateTimeShortcuts.js"]
        return forms.Media(js=["admin/js/%s" % path for path in js])

    def __init__(self, attrs=None, format=None):
        final_attrs = {'class': 'vDateField', 'size': '10'}
        if attrs is not None:
            final_attrs.update(attrs)
        super(AdminDateWidget, self).__init__(attrs=final_attrs, format=format)


class AdminTimeWidget(forms.TimeInput):
    @property
    def media(self):
        js = ["calendar.js", "admin/DateTimeShortcuts.js"]
        return forms.Media(js=["admin/js/%s" % path for path in js])

    def __init__(self, attrs=None, format=None):
        format = "%H:%M"
        final_attrs = {'class': 'vTimeField', 'size': '8'}
        if attrs is not None:
            final_attrs.update(attrs)
        super(AdminTimeWidget, self).__init__(attrs=final_attrs, format=format)


class AdminSplitDateTime(forms.SplitDateTimeWidget):
    """
    A SplitDateTime Widget that has some admin-specific styling.
    """
    def __init__(self, attrs=None):
        widgets = [AdminDateWidget, AdminTimeWidget]
        # Note that we're calling MultiWidget, not SplitDateTimeWidget, because
        # we want to define widgets.
        forms.MultiWidget.__init__(self, widgets, attrs)

    def format_output(self, rendered_widgets):
        return format_html('<p class="datetime">{} {}<br />{} {}</p>',
                           _('Date:'), rendered_widgets[0],
                           _('Time:'), rendered_widgets[1])


class PlayerInput(forms.Widget):

    def render(self, name, value, attrs=None):
        if (value is not None):
            player = Player.objects.get(pk=int(value))
            translations = player.playertranslation_set.all()

            context = {
                "name": name,
                "value": value,
                "title": player.__str__(),
                "translations": translations,
                "image": player.image.url,
            }
            template = loader.get_template('admin/widgets/player_input.html')
            return template.render(context)
        else:
            context = {
                "name": name,
                "value": "",
                "image": "",
            }
            template = loader.get_template('admin/widgets/player_input.html')
            return template.render(context)


class AutoCompleteInput(forms.Widget):

    class Media:
        css = {
            'all': (
                    'admin/css/widgets.css',
            )
        }

        js = ('admin/js/autocomplete_tip.js',)


    def render(self, name, value, attrs=None):

        if (value == None):
            value = ""

        context = {
            "name": name,
            "value": value,
        }

        template = loader.get_template('admin/widgets/autocomplete_tip_input.html')
        return template.render(context)


def dt_now():
    now = datetime.now()
    now = now.replace(minute=0, second=0)
    return now


class ForecastForm(forms.ModelForm):

    class Meta:
        model = Forecast
        fields = '__all__'
        exclude = ("express",)

    # datetime = forms.DateTimeField(widget=AdminSplitDateTime, initial=dt_now)
    lang = forms.ModelChoiceField(Language.objects.all(), label="Язык", required=False, widget=LanguagesSelectInput)
    player_1 = forms.ModelChoiceField(Player.objects.all(), label="Хозяева", widget=PlayerInput)
    player_2 = forms.ModelChoiceField(Player.objects.all(), label="Гости", widget=PlayerInput)


class TipForm(forms.ModelForm):

    class Meta:
        model = Tip
        #fields = ('text',)
        fields = '__all__'

    language = forms.ModelChoiceField(Language.objects.all(), label="Язык", required=True, widget=LanguageInput)
    text = forms.CharField(label="Прогноз", widget=AutoCompleteInput)
