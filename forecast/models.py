from django.db import models
from django.utils import formats
from django.conf import settings
from django.utils import timezone

from express.models import Express
from player.models import Player
from languages.models import Language
from sorl.thumbnail import ImageField

from datetime import datetime
import locale


def localize_month(month, lang):
    month = month-1

    if (lang == "ru"):
        monthes = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]
    else:
        monthes = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

    return monthes[month]


def dt_now():
    now = datetime.now()
    now = now.replace(minute=0, second=0)
    return now


class Forecast(models.Model):
    SPORT_FOOTBALL = '0'
    SPORT_BASKETBALL = '1'
    SPORT_TENNIS = '2'
    SPORT_VOLLEYBALL = '3'
    SPORT_HOCKEY = '3'

    SPORTS_RANGE = range(0, 4)

    SPORT_CHOICES = (
        (SPORT_FOOTBALL, 'Football'),
        (SPORT_BASKETBALL, 'Basketball'),
        (SPORT_TENNIS, 'Tennis'),
        (SPORT_VOLLEYBALL, 'Hockey'),
    )

    class Meta:
        verbose_name = "Forecast"
        verbose_name_plural = "Forecasts"

    sport = models.CharField(
        verbose_name="Sport",
        max_length=1,
        choices=SPORT_CHOICES,
        default='0'
    )

    express = models.ForeignKey(Express, null=True, blank=True)
    player_1 = models.ForeignKey(Player, related_name="player_1",
                                 verbose_name="Home")
    player_2 = models.ForeignKey(Player, related_name="player_2",
                                 verbose_name="Guest")
    odd = models.FloatField("ODD", default=1)
    datetime_create = models.DateTimeField(default=datetime.now, editable=False)
    datetime = models.DateTimeField("Match datetime", default=dt_now)
    free = models.BooleanField("Free", default=True)
    archive = models.BooleanField("Archive", default=False)

    def __str__(self):
        return self.player_1.__str__()+" versus "+self.player_2.__str__()

    def get_tips(self):
        tips = self.tip_set.filter(language="us")
        return tips

    def serialize(self, lang, short=False):
        tips = []
        lang_pk = "us"

        if lang is not None:
            lang_pk = lang

        for tip in self.tip_set.all():
            if tip.language.pk == lang_pk:
                tips.append(tip.serialize())

        day = formats.date_format(timezone.localtime(self.datetime), "d")
        month = int(formats.date_format(timezone.localtime(self.datetime), "m"))
        time = formats.date_format(timezone.localtime(self.datetime), "H:i")

        result = {
            "id": self.pk,
            "odd": self.odd,
            "sport": self.sport,
            "date": localize_month(month, lang_pk)+" "+day,
            "time": time,
            "tips": tips,
        }

        if short:
            result.update({
                "home": self.player_1.pk,
                "away": self.player_2.pk,
            })
        else:
            result.update({
                "home": self.player_1.serialize(lang),
                "away": self.player_2.serialize(lang),
            })

        return result


class ForecastLink(models.Model):
    forecast = models.ForeignKey(Forecast)
    express = models.ForeignKey(Express)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.forecast.express = self.express
        self.forecast.save()

        super(ForecastLink, self).save(force_insert, force_update, using, update_fields)


class Tip(models.Model):

    class Meta:
        verbose_name = "Tip"
        verbose_name_plural = "Tips"

    language = models.ForeignKey(Language, related_name="forecast_lang",
                                 verbose_name="Язык", null=True, blank=True)
    forecast = models.ForeignKey(Forecast)
    text = models.CharField("Подсказка", default="", max_length=1000)

    def __str__(self):
        return ""

    def serialize(self):
        return {
            "id": self.pk,
            "forecast_id": self.forecast.pk,
            "text": self.text,
        }
