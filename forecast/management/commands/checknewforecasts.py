from django.core.management.base import BaseCommand
from django.conf import settings

from notification.models import Notification
import datetime
import time
import sys


class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            while True:
                res = Notification.check_new_forecasts()

                if res:
                    self.stdout.write(self.style.SUCCESS('Send new notification'))

                minutes = 2 if settings.DEMO else 30
                time.sleep(datetime.timedelta(minutes=minutes).seconds)
        except KeyboardInterrupt:
            sys.exit(0)
