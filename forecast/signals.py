from django.contrib.auth.models import User
from forecast.models import Forecast
from notification.models import Notification, NotifySettings
from django.db.models.signals import post_save
from datetime import datetime


def post_save_forecast(sender, **kwargs):
    forecast = kwargs["instance"]
    settings = NotifySettings.objects.all()[:1]
    settings = settings[0]

    if kwargs["created"]:
        settings.last_sport = forecast.sport
        settings.last_send_notify = datetime.now()
        settings.save()

# def post_save_forecast(sender, **kwargs):
#     forecast = kwargs["instance"]
#     settings = NotifySettings.objects.all()[:1]
#     settings = settings[0]
#     title = settings.title
#     body = settings.body
#
#     if kwargs["created"]:
#         # forecas
#         if (forecast.free):
#             Notification.send(
#                 "/topics/global",
#                 "global",
#                 title,
#                 body,
#                 forecast.sport
#             )
#
#         else:
#             for user in User.objects.all():
#                 if (user.userprofile.has_subscribe(forecast.sport)):
#                     Notification.send(
#                         user.userprofile.gcm_token,
#                         user.userprofile.apns_token,
#                         title,
#                         body,
#                         forecast.sport
#                     )

post_save.connect(post_save_forecast, sender=Forecast)
