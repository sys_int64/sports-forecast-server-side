from django.apps import AppConfig


class ForecastConfig(AppConfig):
    name = 'forecast'

    def ready(self):
        from forecast import signals
