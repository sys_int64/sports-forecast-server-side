# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-03-17 14:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forecast', '0005_auto_20160317_1404'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='forecast',
            name='paid',
        ),
        migrations.AddField(
            model_name='forecast',
            name='free',
            field=models.BooleanField(default=False, verbose_name='Бесплатный'),
        ),
    ]
