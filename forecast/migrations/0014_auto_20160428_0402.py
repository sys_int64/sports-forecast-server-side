# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-04-28 04:02
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forecast', '0013_auto_20160427_1114'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='forecast',
            options={'verbose_name': 'Прогноз', 'verbose_name_plural': 'Прогнозы'},
        ),
    ]
