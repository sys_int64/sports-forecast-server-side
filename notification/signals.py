from django.db.models.signals import post_save
from django.dispatch import receiver
from notification.models import Notification


@receiver(post_save, sender=Notification)
def send_notification(sender, **kwargs):
    notify = kwargs["instance"]

    if (notify.title == "" and notify.body == ""):
        return

    Notification.send("/topics/global", "", notify.title, notify.body, -1)
    Notification.send("", "global", notify.title, notify.body, -1)
    notify.title = ""
    notify.body = ""
    notify.save()
