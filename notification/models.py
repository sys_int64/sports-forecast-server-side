import requests
import json
import logging
import time
from datetime import datetime, timedelta
from _ssl import SSLError

from django.utils import timezone
from django.contrib.auth.models import User
from django.db import models
from django.conf import settings
from django.db.models import Q

from push_notifications.models import APNSDevice
from userprofile.models import UserProfile
from apns import APNs, Frame, Payload

logger = logging.getLogger(__name__)


class NotifySettings(models.Model):
    class Meta:
        verbose_name = 'Настройки'
        verbose_name_plural = 'Настройки'

    title = models.CharField("Название", max_length=200)
    body = models.TextField("Сообщение")
    last_send_notify = models.DateTimeField(default=datetime.now)
    last_sport = models.IntegerField(default=-1)


class Notification(models.Model):
    class Meta:
        verbose_name = 'Уведомление'
        verbose_name_plural = 'Уведомления'

    title = models.CharField("Название", max_length=200)
    body = models.TextField("Сообщение", blank=True)

    @staticmethod
    def check_new_forecasts():
        notify_settings = NotifySettings.objects.all()[:1]
        notify_settings = notify_settings[0]
        title = notify_settings.title
        body = notify_settings.body

        now = timezone.now()
        target = notify_settings.last_send_notify+timedelta(minutes=1)

        if now >= target and notify_settings.last_sport >= 0:
            notify_settings.last_sport = -1
            notify_settings.last_send_notify = datetime.now()
            notify_settings.save()

            Notification.send(
                "/topics/global",
                "global",
                title,
                body,
                notify_settings.last_sport
            )

            return True

        return False

    @staticmethod
    def send(gcm, apns, title, body, sport):

        if gcm != "":
            params = {
                "to": gcm,
                "data": {
                    "demo": settings.DEMO,
                    "message": body,
                    "title": title,
                    "sport": sport
                }
            }

            head = {
                "Authorization": "key="+settings.GCM_TOKEN,
                "Content-Type": "application/json"
            }

            requests.post("https://android.googleapis.com/gcm/send",
                          data=json.dumps(params), headers=head)

        if apns == "global":
            try:
                devices = UserProfile.objects.filter(~Q(apns_token="")).values("apns_token")
                devices = devices.distinct()

                server = APNs(use_sandbox=settings.DEMO,
                              cert_file=settings.APNS_CERTIFICATE,
                              key_file=settings.APNS_CERTIFICATE)

                frame = Frame()
                expiry = int(time.time() + 3600)
                priority = 10
                identifier = 0

                for device in devices:
                    identifier += 1
                    token = device["apns_token"]
                    print(token)
                    payload = Payload(alert=body, sound="default", custom={"sport": sport})
                    frame.add_item(token, payload, identifier, expiry, priority)

                server.gateway_server.send_notification_multiple(frame)
                # devices
                # if apns == "global":
                #     apns_devices = APNSDevice.objects.all()
                #     apns_devices.send_message(message=body, sound="default",
                #                               badge=1, extra={"sport": sport})
                # else:
                #     apns_devices = APNSDevice.objects.filter(registration_id=apns)
                #     apns_devices.send_message(message=body, sound="default",
                #                               badge=1, extra={"sport": sport})
            except SSLError as e:
                logger.error("APNS Send notification fail: " + str(e))

        elif apns != "":
            try:
                server = APNs(use_sandbox=settings.DEMO,
                              cert_file=settings.APNS_CERTIFICATE,
                              key_file=settings.APNS_CERTIFICATE)
                payload = Payload(alert=body, sound="default", custom={"sport": sport})
                server.gateway_server.send_notification(apns, payload)
            except SSLError as e:
                logger.error("APNS Send notification fail: " + str(e))
