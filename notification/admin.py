from django.contrib import admin
from singlemodeladmin import SingleModelAdmin
from django.conf import settings
from notification.models import Notification, NotifySettings


class NotificationAdmin(SingleModelAdmin):
    fields = ('title', 'body')


class NotifySettingsAdmin(SingleModelAdmin):
    fields = ('title', 'body', 'last_send_notify', 'last_sport')


admin.site.register(Notification, NotificationAdmin)
admin.site.register(NotifySettings, NotifySettingsAdmin)
