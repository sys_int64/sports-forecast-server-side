from django import forms
from django.contrib.auth.models import User


class UserForm(forms.ModelForm):

    class Meta:
        fields = "__all__"
        model = User

    username = forms.CharField(max_length=75)
