from django.contrib.auth.models import User
from django.db import models
from django.db.models.manager import BaseManager
from django.utils import formats
from datetime import datetime, timedelta, date
from django.db.models.signals import post_save

from express.models import Express
from forecast.models import Forecast
from django.utils import timezone
from django.db.models.query_utils import Q
from dateutil.relativedelta import relativedelta
from django.db.models.signals import class_prepared


class UserProfile(models.Model):
    class Meta:
        permissions = (
            ("can_football_manage", "Can football manage"),
            ("can_basketball_manage", "Can basketball manage"),
            ("can_teniss_manage", "Can teniss manage"),
            ("can_volleyball_manage", "Can volleyball manage"),
        )

    user = models.OneToOneField(User)
    gcm_token = models.CharField(blank=True, max_length=255)
    apns_token = models.CharField(blank=True, max_length=255)
    viewed_forecasts = models.ManyToManyField(Forecast, blank=True)
    left_tips = models.IntegerField("Осталось подсказок", default=0)
    is_device = models.BooleanField(default=False)
    payload = models.CharField(max_length=36, default="", blank=True)

    last_datetime_football = models.DateTimeField(default=datetime.now)
    last_datetime_basketball = models.DateTimeField(default=datetime.now)
    last_datetime_tennis = models.DateTimeField(default=datetime.now)
    last_datetime_volleyball = models.DateTimeField(default=datetime.now)

    subscribe_football = models.DateField(default="2016-01-01")
    subscribe_basketball = models.DateField(default="2016-01-01")
    subscribe_tennis = models.DateField(default="2016-01-01")
    subscribe_volleyball = models.DateField(default="2016-01-01")

    User._meta.get_field("username").max_length = 75

    def update_last_datetime(self, sport):
        params = {
            Forecast.SPORT_FOOTBALL: "last_datetime_football",
            Forecast.SPORT_BASKETBALL: "subscribe_basketball",
            Forecast.SPORT_TENNIS: "subscribe_tennis",
            Forecast.SPORT_VOLLEYBALL: "subscribe_volleyball",
        }

        param = params.get(sport, None)

        if param:
            setattr(self, param, datetime.now())
            self.save()

    def get_forecasts(self, sport, timestamp=None):
        subscribe = False

        if sport is not None:
            if timestamp is not None and self.has_subscribe_timestamp(sport, timestamp):
                query = Q(sport=sport, archive=False)
                subscribe = True
            elif self.has_subscribe(sport):
                query = Q(sport=sport, archive=False)
                subscribe = True
            else:
                query = Q(sport=sport, archive=False, free=True)
        else:
            query = Q(archive=False, free=True)

        viewed_list = self.viewed_forecasts.values_list("id", flat=True)
        query |= (Q(id__in=viewed_list) & Q(sport=sport, archive=False))

        return Forecast.objects.filter(query), subscribe

    def get_expresses(self, timestamp=None):
        return Express.objects.filter(archive=False), False

    def get_free_forecasts(self, sport):
        viewed_list = self.viewed_forecasts.values_list("id", flat=True)
        query = Q(sport=sport, archive=False, free=False)
        query &= ~Q(id__in=viewed_list)

        return Forecast.objects.filter(query)

    def has_subscribe(self, sport):
        try:
            football_subscribe = self.subscribe_football >= date.today() and sport == Forecast.SPORT_FOOTBALL
            basketball_subscribe = self.subscribe_basketball >= date.today() and sport == Forecast.SPORT_BASKETBALL
            tennis_subscribe = self.subscribe_tennis >= date.today() and sport == Forecast.SPORT_TENNIS
            volleyball_subscribe = self.subscribe_volleyball >= date.today() and sport == Forecast.SPORT_VOLLEYBALL

            return football_subscribe or basketball_subscribe or tennis_subscribe or volleyball_subscribe
        except:
            return False

    def get_subsctibe_until(self, sport):
        if sport == Forecast.SPORT_FOOTBALL:
            return self.subscribe_football
        elif sport == Forecast.SPORT_BASKETBALL:
            return self.basketball_subscribe
        elif sport == Forecast.SPORT_TENNIS:
            return self.tennis_subscribe
        elif sport == Forecast.SPORT_VOLLEYBALL:
            return self.volleyball_subscribe
        else:
            return None

    def has_subscribe_timestamp(self, sport, timestamp):
        subscribe_time = datetime.fromtimestamp(float(timestamp)/1000.0)+relativedelta(months=1)

        if (sport == Forecast.SPORT_FOOTBALL):
            self.subscribe_football = subscribe_time.date()
        elif (sport == Forecast.SPORT_BASKETBALL):
            self.subscribe_basketball = subscribe_time.date()
        elif (sport == Forecast.SPORT_TENNIS):
            self.subscribe_tennis = subscribe_time.date()
        elif (sport == Forecast.SPORT_VOLLEYBALL):
            self.subscribe_volleyball = subscribe_time.date()

        self.save()
        return self.has_subscribe(sport)

    @staticmethod
    def get_query_sportperm(request):
        user = request.user
        query = Q()

        if (user.has_perm("userprofile.can_football_manage")):
            query |= Q(sport=Forecast.SPORT_FOOTBALL)

        if (user.has_perm("userprofile.can_basketball_manage")):
            query |= Q(sport=Forecast.SPORT_BASKETBALL)

        if (user.has_perm("userprofile.can_volleyball_manage")):
            query |= Q(sport=Forecast.SPORT_VOLLEYBALL)

        if (user.has_perm("userprofile.can_volleyball_manage")):
            query |= Q(sport=Forecast.SPORT_TENNIS)

        return query

    def serialize(self):
        return {
            "id": self.user.id,
            #"gcm_token": self.gcm_token,
            #"paid_before": formats.date_format(self.paid_before, "DATE_FORMAT"),
            #"last_visit": formats.date_format(self.last_visit, "DATE_FORMAT"),
        }

    def save(self, *args, **kwargs):
        if not self.pk:
            try:
                p = UserProfile.objects.get(user=self.user)
                self.pk = p.pk
            except UserProfile.DoesNotExist:
                pass

        super(UserProfile, self).save(*args, **kwargs)


def create_profile(sender, **kwargs):

    user = kwargs["instance"]

    if kwargs["created"]:
        profile = UserProfile(user=user)
        profile.save()


post_save.connect(create_profile, sender=User)

from django.db.models.signals import class_prepared

def longer_username(sender, *args, **kwargs):
    # You can't just do `if sender == django.contrib.auth.models.User`
    # because you would have to import the model
    # You have to test using __name__ and __module__
    if sender.__name__ == "User" and sender.__module__ == "django.contrib.auth.models":
        sender._meta.get_field("username").max_length = 75

class_prepared.connect(longer_username)
