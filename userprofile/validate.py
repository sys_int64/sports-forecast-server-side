from oauth2_provider.models import get_application_model, AbstractApplication


def validate_client(client_id, client_secret):

    Application = get_application_model()

    try:
        app = Application.objects.get(client_id=client_id)

        if (client_secret != app.client_secret):
            return False

    except Application.DoesNotExist:
        return False

    return True
