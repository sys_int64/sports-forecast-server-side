from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User, Group
from userprofile.models import UserProfile
from userprofile.forms import UserForm


class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'userprofile'


class CustomUserAdmin(UserAdmin):
    list_display = ("username", )

    # def get_queryset(self, request):
    #     qs = super(CustomUserAdmin, self).get_queryset(request)
    #     return qs.filter(userprofile__is_device=False)
    form = UserForm
    inlines = (UserProfileInline, )


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
# admin.site.unregister()
