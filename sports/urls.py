from django.conf.urls import url
from sports import views


urlpatterns = [
    url(r'^terms/$', views.TermsView.as_view(), name="terms"),
    url(r'^services-description-and-cancellation-policy/$', views.ServicesDescView.as_view()),
    url(r'^cancellation/$', views.CancellationView.as_view()),
    url(r'^football/$', views.SportDetails.as_view(sport_name="football")),
    url(r'^basketball/$', views.SportDetails.as_view(sport_name="basketball")),
    url(r'^tennis/$', views.SportDetails.as_view(sport_name="tennis")),
    url(r'^volleyball/$', views.SportDetails.as_view(sport_name="volleyball")),
    url(r'^payment/status-change/$', views.payment_callback),
    url(r'^payment/unlock/$', views.PaymentUnlock.as_view()),
    url(r'^$', views.SportDetails.as_view(sport_name="football")),
]
