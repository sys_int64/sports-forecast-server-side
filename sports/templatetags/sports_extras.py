from django import template
register = template.Library()


def get_lang_tips(forecast, lang):
    tips_lang = []
    lang_pk = "ru"

    if (lang != None):
        lang_pk = lang

    for tip in forecast.tip_set.all():
        if (tip.language.pk == lang_pk):
            tips_lang.append(tip)

    return tips_lang


def get_player_lang(player, lang):
    translation = player.get_translation(lang)

    if (translation == None):
        title = ""
    else:
        title = translation.title

    return title

register.filter('get_player_lang', get_player_lang)
register.filter('get_lang_tips', get_lang_tips)
