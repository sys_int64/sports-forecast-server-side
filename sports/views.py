from django.contrib import messages
from django.core.mail import send_mail
from django.http import HttpResponseRedirect
from phonenumber_field.phonenumber import PhoneNumber
from phonenumbers import NumberParseException

from history.models import History
from forecast.models import Forecast
from django.views.generic import TemplateView
from api.rate.get import RateGet

from datetime import timedelta, datetime
from django.views.generic import View
from django.http import JsonResponse

from hashlib import sha512, sha1
from hmac import HMAC
from django.db.models import Sum
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.contrib.auth.models import User
from dateutil.relativedelta import relativedelta

import requests
import json


def move_archive_expired():
    time_threshold = datetime.now()-timedelta(hours=4)
    forecasts_query = Forecast.objects.filter(datetime__lt=time_threshold,
                                              archive=False)

    for forecast in forecasts_query.all():
        forecast.archive = True
        forecast.save()
        History.objects.create(forecast=forecast)


def generate_signature(uri, secret, nonce='', body='', method='GET'):
    bindata = (nonce + body).encode()
    digest  = sha512(bindata).digest()
    message = (method + uri).encode() + digest
    secret  = secret.encode()

    h = HMAC(secret, message, 'sha512')
    #~ hmachex = h.hexdigest()
    #hmacb64 = b64encode(h.digest())

    return h
    # method = 'GET'
    # sha512 = hashlib.sha512()
    # #sha512.update("".encode("utf-8"))
    # request = method+request_uri+str(sha512.hexdigest())+"\0"
    # print(request)
    # #hmac_ = hmac.new(bytes(secret.encode("utf-8")), bytes(request.encode("utf-8")), hashlib.sha512)
    # hmac_ = hmac.new(bytes(secret.encode("utf-8")), bytes(request.encode("utf-8")), hashlib.sha512)
    # print(hmac_.hexdigest())
    # return base64.b64encode(hmac_.digest())


def cryptopay_signature(api_key, uuid, cents, currency):
    data = "{}_{}_{}{}".format(api_key, uuid, cents, currency)
    return sha1(data.encode()).hexdigest()


class TermsView(TemplateView):
    template_name = "terms.html"


class ServicesDescView(TemplateView):
    template_name = "services_desc.html"


class CancellationView(TemplateView):
    template_name = "cancellation.html"


class PaymentUnlock(View):

    def post(self, request):

        if not request.user.is_authenticated():
            return JsonResponse({
                "error": "Need authentication"
            })

        sport_id = request.POST.get("sport")
        print(sport_id)

        if request.user.userprofile.has_subscribe(sport_id):
            return JsonResponse({
                "error": "You already have subscription"
            })

        data = {
            "id": settings.CRYPTOPAY_CUSTOM_ID,
            "price": 10,
            "currency": "USD",
            "callback_url": "http://revengebet.com/payment/status-change/",
            "success_redirect_url": "http://revengebet.com",
            "callback_params": "{\"user_id\":"+str(request.user.pk)+",\"sport_id\":\""+str(sport_id)+"\"}"
        }

        headers = {
            "X-Api-Key": settings.CRYPTOPAY_API_KEY
        }

        r = requests.post(
            "https://cryptopay.me/api/v2/invoices",
            data=data,
            headers=headers,
        )

        return JsonResponse(json.loads(r.text))
        #return JsonResponse({
        #    "btc_address": "2N4ZXn7JU7C8yH4c6tFwy61HaB7LmNpECif",
        #    "btc_price": 0.00215,
        #    "bitcoin_uri": "bitcoin:2N4ZXn7JU7C8yH4c6tFwy61HaB7LmNpECif",
        #})


@csrf_exempt
def payment_callback(request):
    data = json.loads(request.body.decode())
    print(data)

    uuid = data["uuid"]
    currency = data["currency"]
    price = data["price"]
    validation_hash = data["validation_hash"]
    callback_params = json.loads(data["callback_params"])
    print(callback_params)
    user_id = int(callback_params["user_id"])
    sport = callback_params["sport_id"]
    cents = int(price*100)

    print(sport)
    print(uuid)
    print(currency)
    print(price)

    signature = cryptopay_signature(settings.CRYPTOPAY_API_KEY, uuid, cents, currency)

    print(signature)
    print(validation_hash)

    if (signature == validation_hash):

        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            print("user not found")
            return JsonResponse({"error": "User not found"})

        if (sport == Forecast.SPORT_FOOTBALL):
            now = user.userprofile.subscribe_football

            if (now < datetime.now().date()):
                now = datetime.now().date()

            d = now+timedelta(days=1)
            d += relativedelta(months=1)
            user.userprofile.subscribe_football = d

        elif (sport == Forecast.SPORT_BASKETBALL):
            now = user.userprofile.subscribe_basketball

            if (now < datetime.now().date()):
                now = datetime.now().date()

            d = now+timedelta(days=1)
            d += relativedelta(months=1)
            user.userprofile.subscribe_basketball = d

        elif (sport == Forecast.SPORT_TENNIS):
            now = user.userprofile.subscribe_tennis

            if (now < datetime.now().date()):
                now = datetime.now().date()

            d = now+timedelta(days=1)
            d += relativedelta(months=1)
            user.userprofile.subscribe_tennis = d

        elif (sport == Forecast.SPORT_VOLLEYBALL):
            now = user.userprofile.subscribe_volleyball

            if (now < datetime.now().date()):
                now = datetime.now().date()

            d = now+timedelta(days=1)
            d += relativedelta(months=1)
            user.userprofile.subscribe_volleyball = d

        user.userprofile.save()

        print("ok")
        return JsonResponse({"ok": True})
    else:
        print("Error!")
        return JsonResponse({"error": "signature check error"})


class SportDetails(TemplateView):
    template_name = "details.html"
    sport_name = "football"
    phone_error = False

    def get_context_data(self, **kwargs):
        sport = {
            "football": Forecast.SPORT_FOOTBALL,
            "basketball": Forecast.SPORT_BASKETBALL,
            "tennis": Forecast.SPORT_TENNIS,
            "volleyball": Forecast.SPORT_VOLLEYBALL,
        }[self.sport_name]

        move_archive_expired()
        history = History.objects.filter(forecast__sport=sport, price__gt=0).order_by("-forecast__datetime")[:100]
        subscription_until = None

        try:
            if self.request.user.is_authenticated() and self.request.user.userprofile.has_subscribe(sport):
                forecasts = Forecast.objects.filter(sport=sport, archive=False)
                subscription_until = self.request.user.userprofile.get_subsctibe_until(sport)
            else:
                forecasts = Forecast.objects.filter(sport=sport, archive=False, free=True)
        except:
            forecasts = Forecast.objects.filter(sport=sport, archive=False, free=True)

        history_rate = History.objects.filter(forecast__sport=sport, price__gt=0).order_by("forecast__datetime")
        rate = RateGet.rate(history_rate, True).get("rate_set")
        total_sports = 0

        if History.objects.all().count() > 0:
            win_qs = History.objects.filter(price__gt=0, win=True)
            lose_qs = History.objects.filter(price__gt=0, win=False)

            if win_qs.count() > 0:
                total_sports = win_qs.aggregate(Sum('price'))['price__sum']

            if lose_qs.count() > 0:
                total_sports -= lose_qs.aggregate(Sum('price'))['price__sum']
        else:
            total_sports = 0

        def is_empty(val):
            return val is None or val.strip() == ""

        name = ""
        phone = ""
        email = ""
        confirm = ""

        name_error = ""
        email_error = ""
        phone_error = ""
        confirm_error = ""

        if self.request.POST.get("feedback_submit") is not None:
            name = self.request.POST.get("name")
            phone = self.request.POST.get("tel")
            email = self.request.POST.get("email")
            confirm = self.request.POST.get("confirm")

            name_error = "has-error" if is_empty(name) else ""
            email_error = "has-error" if is_empty(email) else ""
            phone_error = "has-error" if self.phone_error else ""
            confirm_error = "has-error" if is_empty(confirm) else ""

            if name is None:
                name = ""

            if phone is None:
                phone = ""

            if email is None:
                email = ""

            if not is_empty(confirm):
                confirm = "checked"

        return {
            'name': "football",
            'richer': History.calc_total(sport),
            'sport': self.sport_name,
            'sport_id': sport,
            'forecasts': forecasts,
            'history': history,
            'total_sports': round(total_sports),
            'rate': rate,
            'subscription_until': subscription_until,

            "feedback_post_name": name,
            "feedback_post_email": email,
            "feedback_post_tel": phone,
            "feedback_post_confirm": confirm,

            "feedback_name_error": name_error,
            "feedback_email_error": email_error,
            "feedback_tel_error": phone_error,
            "feedback_confirm_error": confirm_error,
        }

    def post(self, request):
        def is_empty(val):
            return val is None or val.strip() == ""

        name = request.POST.get("name")
        phone = request.POST.get("tel")
        email = request.POST.get("email")
        confirm = request.POST.get("confirm")
        error = False

        if is_empty(name) or is_empty(email) or is_empty(confirm):
            error = True

        if not is_empty(phone):
            try:
                PhoneNumber.from_string(phone)
            except NumberParseException:
                self.phone_error = True
                error = True
        else:
            self.phone_error = True
            error = True

        if error:
            messages.error(request, "Sorry, there's a slight problem with those details<br>"
                                    "Please check below and try again")

        if not error:
            to = ["admin@revengebet.com",
                  "a@theinvaders.ru",
                  "Ekorniushina@gmail.com"]
            subject = "RevengeBET subscription"
            message = "From RevengeBET was sent email\nname: {}\nemail: {}\nphone:{}" \
                .format(name, email, phone)

            res = send_mail(subject, message, "robot@revengebet.com", to, fail_silently=False)

            if not res:
                messages.error(request, "Unexpected error")
            else:
                # messages.success(request, "Thank you! Please check you email!")
                messages.success(request, "Successfully, thank you!")
                return HttpResponseRedirect(request.get_full_path() + "#feedback-form")

        return super(SportDetails, self).get(request)
