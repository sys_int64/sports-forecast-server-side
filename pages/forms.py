from django import forms
from string import Template
from django.utils.safestring import mark_safe
from pages.models import AboutPage, AboutPageTranslation
from django.template import loader
from languages.forms import LanguagesSelectInput, LanguageInput
from languages.models import Language


class AboutPageForm(forms.ModelForm):

    class Meta:
        model = AboutPage
        fields = '__all__'

    lang = forms.ModelChoiceField(Language.objects.all(), label="Язык",
                                  required=False, widget=LanguagesSelectInput)


class AboutPageTranslationForm(forms.ModelForm):

    class Meta:
        model = AboutPageTranslation
        #fields = '__all__'
        fields = ('language', 'text')

    language = forms.ModelChoiceField(Language.objects.all(), label="Язык",
                                      required=True, widget=LanguageInput)
