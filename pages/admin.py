from django.contrib import admin
from pages.models import AboutPage, AboutPageTranslation
from languages.admin import SingleLanguageInline
from pages.forms import AboutPageForm, AboutPageTranslationForm
from singlemodeladmin import SingleModelAdmin


class TranslationInline(SingleLanguageInline):
    model = AboutPageTranslation
    extra = 0
    form = AboutPageTranslationForm
    max_num = 999


class AboutPageAdmin(SingleModelAdmin):
    #list_display = ('sport',)
    #list_filter = ['sport',]
    #search_fields = ['title', 'text']
    inlines = [TranslationInline]
    form = AboutPageForm


admin.site.register(AboutPage, AboutPageAdmin)
