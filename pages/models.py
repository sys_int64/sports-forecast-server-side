from django.db import models
from sorl.thumbnail import ImageField
from languages.models import Language


class AboutPage(models.Model):

    class Meta:
        verbose_name = "О сервисе"
        verbose_name_plural = "О сервисе"

    image = ImageField("Изображение", null=True)

    def serialize(self):
        return {
            "image": self.image.url,
            "text" : self.text,
        }


class AboutPageTranslation(models.Model):

    page = models.ForeignKey(AboutPage)
    language = models.ForeignKey(Language, related_name="about_page_lang",
                                 verbose_name="Язык", null=True, blank=True)
    text = models.TextField("Содержание", blank=True)
